<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>us.javaj</groupId>
  <artifactId>magnus</artifactId>
  <version>0.9-SNAPSHOT</version>
  <packaging>war</packaging>

  <name>Project Magnus</name>
  <inceptionYear>2018</inceptionYear>
  <description>Web app ideas and a reboot of the 20-year old classic, King Floyd's Empire!</description>
  <organization>
    <name>JavaJ LLC</name>
    <url>http://javaj.us</url>
  </organization>

  <developers>
    <developer>
      <name>Justin Peck</name>
      <url>http://javaj.us</url>
      <email>justin@javaj.us</email>
      <roles>
        <role>Web Application Architect</role>
        <role>Vinyl Record Adherent</role>
        <role>Political Pundit</role>
      </roles>
    </developer>
  </developers>

  <!-- Spring Boot! -->
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.3.3.RELEASE</version>
  </parent>

  <properties>
    <build.host>${magnus-host}</build.host>
    <build.database>${magnus-database}</build.database>
    <build.webapp.user>${magnus-user}</build.webapp.user>
    <build.webapp.password>${magnus-password}</build.webapp.password>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
    <java.version>11</java.version>
  </properties>

  <distributionManagement>
    <repository>
      <id>releases</id>
      <url>http://localhost:8081/repository/maven-releases</url>
    </repository>
    <snapshotRepository>
      <id>snapshots</id>
      <url>http://localhost:8081/repository/maven-snapshots</url>
    </snapshotRepository>
  </distributionManagement>

  <dependencies>
    <!-- Spring starters -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-freemarker</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-tomcat</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-validation</artifactId>
    </dependency>

    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-configuration-processor</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <optional>true</optional>
    </dependency>
    <dependency>
      <groupId>org.springframework.session</groupId>
      <artifactId>spring-session-jdbc</artifactId>
    </dependency>

    <!-- Testing -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <exclusions>
        <exclusion>
          <groupId>org.mockito</groupId>
          <artifactId>mockito-core</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-api-mockito2</artifactId>
      <version>2.0.2</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.powermock</groupId>
      <artifactId>powermock-module-junit4</artifactId>
      <version>2.0.2</version>
      <scope>test</scope>
    </dependency>

    <!-- Everything else -->
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate-validator</artifactId>
      <version>6.0.20.Final</version>
    </dependency>
    <dependency>
      <groupId>javax.validation</groupId>
      <artifactId>validation-api</artifactId>
    </dependency>
    <dependency>
      <groupId>io.github.gitbucket</groupId>
      <artifactId>markedj</artifactId>
      <version>1.0.16</version>
    </dependency>
    <dependency>
      <groupId>mysql</groupId>
      <artifactId>mysql-connector-java</artifactId>
      <scope>runtime</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-fileupload</groupId>
      <artifactId>commons-fileupload</artifactId>
      <version>1.4</version>
    </dependency>
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
    </dependency>
  </dependencies>

  <build>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <configuration>
          <encoding>UTF-8</encoding>
          <source>${java.version}</source>
          <target>${java.version}</target>
        </configuration>
      </plugin>
      <!-- Torque Schema Generator -->
      <plugin>
        <groupId>org.apache.torque</groupId>
        <artifactId>torque-maven-plugin</artifactId>
        <version>3.3</version>
        <configuration>
          <!-- torque:sql -->
          <runOnlyOnSchemaChange>false</runOnlyOnSchemaChange>
          <schemaIncludes>*-schema.xml</schemaIncludes>
          <schemaDir>src/main/config/schema</schemaDir>
          <templatePath>src/main/config/torque/templates</templatePath>
          <targetDatabase>mysql</targetDatabase>
          <!-- torque:sqlExec -->
          <driver>com.mysql.jdbc.Driver</driver>
          <url>jdbc:mysql://${host}:3306/${database}</url>
          <user>${user}</user>
          <password>${password}</password>
        </configuration>
        <dependencies>
          <dependency>
            <groupId>torque</groupId>
            <artifactId>torque-gen</artifactId>
            <version>3.3</version>
          </dependency>
          <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.17</version>
          </dependency>
        </dependencies>
      </plugin>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>sql-maven-plugin</artifactId>
        <version>1.5</version>
        <configuration>
          <!-- sql:execute -->
          <url>jdbc:mysql://${host}:3306/${database}</url>
          <driver>com.mysql.jdbc.Driver</driver>
          <username>${user}</username>
          <password>${password}</password>
          <srcFiles>
            <srcFile>src/main/config/scripts/booknook-data.sql</srcFile>
            <srcFile>src/main/config/scripts/magnus-data.sql</srcFile>
            <srcFile>src/main/config/scripts/player-data.sql</srcFile>
            <srcFile>src/main/config/scripts/session-schema.sql</srcFile>
          </srcFiles>
        </configuration>
        <dependencies>
          <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.17</version>
          </dependency>
        </dependencies>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <skipTests>true</skipTests>
        </configuration>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-deploy-plugin</artifactId>
      </plugin>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

  <!--
  <repositories>
    <repository>
      <id>spring-milestones</id>
      <url>http://repo.spring.io/milestone</url>
    </repository>
  </repositories>

  <pluginRepositories>
    <pluginRepository>
      <id>spring-milestones</id>
      <url>http://repo.spring.io/milestone</url>
    </pluginRepository>
  </pluginRepositories>
  -->

  <profiles>
    <!--+
        | Call database mojos using these profiles with examples to follow:
        |
        | mvn torque:sql
        | mvn torque:sqlExec -Pmagnus
        | mvn sql:execute -Pmagnus
        +-->
    <profile>
      <id>magnus</id>
      <properties>
        <host>${build.host}</host>
        <database>${build.database}</database>
        <user>${build.webapp.user}</user>
        <password>${build.webapp.password}</password>
      </properties>
    </profile>
  </profiles>
</project>
