This is the "magnus" Git repository, home to the JavaJ development blog and
general information Spring Boot web application and all its associated files.

To build all the needed site documentation, you can please do the following:

 * Open a command shell and change to the project's root directory
 * Be sure you have Maven installed and enter the command "mvn site"
 * Open up [PROJECT_ROOT]/target/site/index.html in your web browser

Enjoy!
