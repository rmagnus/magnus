/**
 * --------------------------------------------------------------------------
 * JavaJ LLC: Record.js
 * Mithril component for dynamic vinyl records 'views'
 * --------------------------------------------------------------------------
 */
var bmg = 'background-image: url(https://cdn.pixabay.com/photo/2012/04/13/13/23/disc-32390_960_720.png)';

var Record = {
  oninit: function(vnode) {
    this.data = vnode.attrs;
  },
  view: function(vnode) {
    return m("div.media-body", [
        m("div.media-heading",
          m("h6", [ // sibling title and edit link
            m("a", {href: "#"}, this.data.title),
            m("small",
              m("span", m.trust("&nbsp;&middot;&nbsp;" + this.data.artist.name),
                ) ) ] ) ),
        m("div.mt-2",
          m("div.row", [
            m("div.col-lg-5",
              m("div.card card-profile mb-2", [
                m("div.card-header", {style: bmg}),
                m("div.card-body",
                  m("ul.list-unstyled list-spaced", [ // record label
                    m("li",
                      m("span.text-muted icon icon-wallet",
                        m("span.ml-2", this.data.label)),
                          ),
                    m("li",
                      m("span.text-muted icon icon-calendar",
                        m("span.ml-2", "Released " + this.data.releaseYear)),
                          ),
                    m("li.tracklist-link",
                        m("span.icon icon-list",
                          m("span.ml-2", m("a", {href: "#", "data-id": this.data.id}, "Show Tracklist"))),
                            ),
                      ] ) ) ] ) ),
            m("div.col-lg-7 tracklist",
              m("span.text-muted", "Tracklist")
                ) ] ) ) ] );
  }
}