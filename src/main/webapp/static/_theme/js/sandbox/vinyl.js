
/**
 * --------------------------------------------------------------------------
 * JavaJ LLC: vinyl.js
 * Mithril scripts for dynamic vinyl records display
 * --------------------------------------------------------------------------
 */

m.request({
  method: "GET",
  url: "/vinyl/records/:id",
  params: {id: 4},
})
.then(function(data) {
  //m.mount(element, {view: function () {return m(Component, attrs)}})
  //m.render(document.getElementById('mithril'), m(Record, data));
  
  m.mount(document.getElementById('mithril'), {view: function() {return m(Record, data)}});
  console.log(data);
})




