
/**
 * --------------------------------------------------------------------------
 * JavaJ LLC: projects.js
 * Scripts for Projects section of the application
 * --------------------------------------------------------------------------
 */

$(function(){

  $("#projects-link").click(function() {
    $("#booknook").addClass('d-none');
    $("#projects").removeClass('d-none');
  })

  $("#sandbox-link").click(function() {
    $("#projects").addClass('d-none');
    $("#booknook").removeClass('d-none');
  })

  /*
  $("body").on("click", "li.tracklist-link a", function() {
    console.log($(this).data('id'));
  })
  */
})