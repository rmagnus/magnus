/*
 Copyright (c) JavaJ 2020
 Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */
$(function() {

  $("#avatarFile,#backgroundFile").change(function(evt) {
    var button = $(this).prev("button");
    var fakepath = $(this).val();
    var filename = fakepath.replace(/^.*\\/, "");
    if (filename.length == 0) {
      button.text("Upload Image");
      return resetImage(button.data('type'));
    }
    button.text(filename);
    uploadImage(button.data('type'), $(this));
  });

  function uploadImage(type, elem) {
    var formData = new FormData();
    var files = $(elem).prop('files');
    formData.append('image', files[0]);
    formData.append('type', type);

    axios({
      url: '/profile/upload',
      method: 'post',
      data: formData,
      headers: {
        "content-type": "multipart/form-data"
      }
    })
    .then(function(response) {
      if (200 == response.status) {
        displayImage(type, response.data);
      }
    });
  }

  function displayImage(type, url) {
    var elem = $("#" + type + "-image");
    console.log("this is what to download: " + url);
    if (type == 'avatar') {
      elem.attr('src', url);
    }
    else { // background image
      var css = 'url(' + url + ')';
      elem.css("background-image", css);
    }
  }

  function displayDefault(type) {
    var elem = $("#" + type + "-image");
    if (type == 'avatar') {
      elem.attr('src', elem.data('default'));
    }
    else { // background image
      var css = 'url(' + elem.data('default') + ')';
      elem.css("background-image", css);
    }
  }

  function resetImage(type) {
    var formData = new FormData();
    formData.append('type', type);

    axios({
      url: '/profile/reset',
      method: 'post',
      data: formData
    })
    .then(function(response) {
      if (200 == response.status) {
        displayDefault(response.data);
      }
    });
  }
});
