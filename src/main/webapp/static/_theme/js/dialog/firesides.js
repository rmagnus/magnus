/*
 Copyright (c) JavaJ 2020
 Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */
$(function() { 

  $("#fireside-modal").on('show.bs.modal', function(evt) {
    var title = $(evt.relatedTarget).prev("span").text();
    $("#fireside-title").text(title);
  });

});
