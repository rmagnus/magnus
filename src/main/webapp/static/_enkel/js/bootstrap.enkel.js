/********************************************************
 *
 * Custom Javascript code for Enkel Bootstrap theme
 * Written by Themelize.me (http://themelize.me)
 *
 *******************************************************/
$(document).ready(function() {
  
  // Bootstrap tooltip
  // ----------------------------------------------------------------
  // invoke by adding _tooltip to a tags (this makes it validate)
  $('body').tooltip({
    selector: "a[class*=_tooltip]"
  });
    
  // Bootstrap popover
  // ----------------------------------------------------------------
  // invoke by adding _popover to a tags (this makes it validate)
  $('body').popover({
    selector: "a[class*=_popover]",
    trigger: "hover"
  });

  //flexslider
  // ----------------------------------------------------------------
  $('.flexslider').each(function() {
    var sliderSettings = {
      animation: $(this).data('transition'),
      selector: ".slides > .slide",
      controlNav: true,
      smoothHeight: true,
      animationLoop: false,
      slideshowSpeed: 7000
    };

    /*
    var sliderNav = $(this).attr('data-slidernav');
    if (sliderNav !== 'auto') {
      sliderSettings = $.extend({}, sliderSettings, {
        manualControls: sliderNav +' li a',
        controlsContainer: '.flexslider-wrapper'
      });
    }
    */

    $(this).flexslider(sliderSettings);

  });

});