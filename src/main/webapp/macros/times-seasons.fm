<#macro part1>
<h1>Augustus</h1>

<p>This one was hard. A time of renewing, a time to break down and cast away stones.</p>

<p>August 2020 was an 'interesting' time for me, to say the least. Early in the month I was busy <em>trying to work</em>
as I had always done, as was my custom so to speak &mdash; when all of a sudden my mind shut down. It &hellip; just &hellip;
shut down hard; there was no choice in the matter. It happened sometime during the second and third weeks, and what is
more is, I was saved by the grace of so many friends &amp; family and medical professionals that came to my rescue,
again so to speak. These were the&hellip;</p>

<div class="docs-example">
  <h2>
    <span>Times and Seasons &mdash; Part One</span>
    <button class="m-1 btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#fireside-modal">
      <span class="icon icon-user"></span> Share This
    </button>
  </h2>
</div>
<div class="highlight"><pre><code class="language-html" data-lang="html"><span class="cm">This one is troubling; such as it is.</span>

John the Revelator was tired. He'd finished a number of letters to the churches
and now needed to rest. Yet there was still so much to do! <span class="gb">‘I must finish these
messages to the brethren! They need this now, today. They need to know that God
is aware of their hardships and struggles.’</span>

<span class="sd">“For we wrestle not against flesh and blood, but against principalities,
   against powers, against the rulers of the darkness of this world,
      against spiritual wickedness in high places.”</span> <small class="s1">(Ephesians 6:12)</small>

<span class="na">&mdash;</span>

FIRE! DESOLATION! Heinous images and more filled his weary mind. <span class="gb">‘What is this
new devilry?’</span> he wrestled. Additional emotion rushed his heart, seeming to hang
over the head of his beloved brothers and sisters! And there just wasn’t enough
time to do it all. Or so it seems(ed). <span class="gb">‘How do I warn them?!’</span> contemplated The
Revelator. Just then a vision unfolded before his eyes. He had witnessed this
before, on an isle called Patmos. The gift of PROPHECY was his, The Lamb of God
speaking to his heart and mind. The words flowed deftly; this time the cursive
was harrowing:

<span class="sd">“And when he <span class="cm">[I AM, The Lamb of God]</span> opened the seventh seal, there was silence
in heaven for about half an hour. I saw seven angels who stand before God, and
they were given seven trumpets. And another angel came and stood at the altar
having a golden incense burner <span class="cm">[filled to the brim with incense representing]</span>
the prayers of the saints &hellip; <span class="cm">[all this]</span> was before the throne <span class="cm">[of our God]</span>.”</span>

<span class="na">&mdash;</span>

<span class="sd">“And the smoke of incense <span class="cm">[representing collective prayer]</span> arose from the hand
of the angel before God. And the angel took the incense burner and filled it
with fire from the altar &mdash; and threw it to the earth. <span class="cm">[And]</span> there were peals
of thunder and rumblings, lightning flashes, and an earthquake.”</span> <small class="s1">(Revelation 8:1-5)</small>

<span class="gb">‘Why now?’</span> thought John. <span class="gb">‘Is this &hellip; now?!’</span> thought John (or Justin &hellip; or Will
&hellip; or Scott &hellip; and many, MANY others). Let’s fast forward to today, to our day,
around nineteen centuries later. Why does the imagery of REVELATION play before
my mind TODAY?

A friend of mine recently asked the question when returning from Heber Valley's
Wasatch Mountain State Park (it was a golf day!..): <span class="gi">‘What does God mean by half
an hour of silence?’</span> we pondered that day. Was this to occur before the ‘shat-
tering of the world?’ Perhaps Richard Draper can shed some light on what awaits
the world in the coming days and weeks ahead&hellip;

<span class="na">&mdash;</span>

Silence in HEAVEN: As the seal is broken, there is silence in heaven for about
the space of half an hour. A solemn moment [in heaven] intervenes before JUDGE-
MENT commences. Nothing breaks the hush, but tension builds as SEVEN angels
[captains over a host!] receive SEVEN trumpets and prepare to give their
commands. [Soon] their BLASTS will signal the onslaught of those forces that
will shatter the world. But an interval is needed: a period of preparation,
of devotion and worship, of individual readying before heaven executes God’s
WILL; a moment of mourning for a world sick unto death [e.g. COVID-19 post
Nineteen Centuries (as illustrated above, emphasis added)]. <small>(New Testament Commen-
tary, The Revelation of John the Apostle)</small>

<span class="hl">It is my conviction, brothers and sisters, that the season is NOW; it is today.
The good news (gospel of Jesus Christ) is that you and I have been given TIME
to prepare &mdash; let’s do so!</span>

<span class="na">&mdash;</span>

<span class="sd">“For behold, this life is the time for men to prepare to meet God; yea, behold
the day of this life is the day for men to perform their labors.”</span>  <small class="s1">(Alma 34:32)</small>

<span class="hl">We must all now today stand in holy places; learn how to step aside and APART
from the noise of the WORLD. For a time, let us get off Facebook, InstaGram,
and TikTok, please. I invite us all to do so.</span> The NOISE is found all around us;
in media, in city streets, in bars, in raucous music, on highjacked roads and
senseless riots. Is this not true? Please open your heart this day to receive
this fireside message invitation. Thank you.

<span class="na">&mdash;</span>

We have been given by our Father in Heaven this little season before SEVEN
calamities are to unleash upon the world. Missionaries and the priesthood of
God (144k strong!) are preparing TODAY. At the same time, 15 Apostles of the
Lamb are praying in the temples, as we saints anxiously await the day when we
can return to worship our God. I believe this to be true. The question I pose
to you today is, <span class="gi">‘What do you believe?’</span> I wonder greatly. Could be I’m just up
in the night (but I doubt it). And while it may be true, many of us sleepless
and up late into the night, my point is this: <span class="gi">Are we pondering these event as
we should be doing? Are we studying the scriptures? Are we really understanding
the message of John the Revelator?</span> Sure, sure it was written [COVID] nineteen
centuries ago. So what? The Spirit of Revelation is alive, in you, in me, but
only as we learn how to HEAR HIM.

<span class="na">&mdash;</span>

<span class="hl">The number SEVEN (trumpets) in apocalyptic scripture represents complete(ness).
It is wholeness; could even say goodness. Conversely applied, if you and I are
complete, then we shall not fear. As we seek to be so, I promise us in the name
of the Lord Jesus Christ: We shall not worry about elections, red states nor
blue, Chuck or Nancy, Biden or Trump, the monkey nor the thing. You and I can
(and should!) read the above epistle to mean a TIME has been allotted.</span> For what
you may ask? According to Draper, my numero uno trusted source for all things
REVELATION, to withstand the complete shattering of the world as we know it. In
scripture, this is known as the Fall of Babylon. Stay tuned&hellip;

Next week in part two of Times and Seasons, explore with me the beast of
REVELATION and the fire (lie) breathing dragon, and discover the true meaning
of the marks the beast leaves upon the unwary. Are we aware? We can be (it’s
our choice afterALL). Let's put on the ARMor of GOD. Why not arm yourself(?!)
during the coming weeks and months &mdash; in order to completely stand separate and
apart from a world sick with sin, soon to shatter. It’s gonna be a rip-roaring,
crazy GOOD time&hellip;!

God bless America. God bless his Saints in American and abroad, throughout all
the world. The earth is His creation. Sure, while we are small in number, we
are great in Faith, Hope, Charity &hellip; and LOVE(!). All this and more in the name
of The Lamb of God, He who was slain, the great I AM, even Jesus Christ, amen.
</pre></code></div>
</#macro>



<#macro part2>
<h1>Plagues, Pestilences, Pests and Pandemics</h1>

<p>Richard D. Draper has been my go-to resource for all things Revelation. He and author Michael D. Rhodes
have gone to painstaking lengths to retranslate the entire Revelation of St. John &mdash;
from the earliest Greek available.</p>

<p>In this <a href="https://www.byunewtestamentcommentary.com/plagues-pestilences-pests-and-pandemics-in-these-latter-days/">
article penned by Draper</a>, he discusses this massive undertaking, as well as points out the meaning of
'pestilences' in scripture. For those concerned about the sheer magnitude of destruction (or de-creation)
to occur in the last days, there is hope. From the article:</p>

<p>"The work of the team of scholars who are preparing the BYU New Testament Commentary volumes
has made us very aware that there is no doubt that we all are in for a rough time, but the
righteous will be spared from the worst of it." <small>(Draper)</small></p>

<div class="docs-example">
  <span>Times and Seasons &mdash; Part Two</span>
  <button class="m-1 btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#fireside-modal">
    <span class="icon icon-user"></span> Share This
  </button>
</div>

<div class="highlight"><pre><code class="language-html" data-lang="html">Many fear the BOOK of Revelation, I get it. I understand perfectly as I was one
of those who did. I mean, the very nature of apocalyptic scripture asks us to
face life’s finality &mdash; it’s end if you will. I for one am no fan of finality.
Death isn’t particularly wonderful to contemplate, speaking personally. That
said, much of the book is rich with HOPE and divine PROMISE. And so once again,
I greatly desire to delve deeper into its versus with you in order to LOOK for
applicable knowledge. Here we go&hellip;!

“It is no exaggeration to describe the book of Revelation as a story of horrors
and destructions. The book’s imagery neither soft-pedals nor downplays the
trouble the world is now moving into.

“And yet, through the whole dark and fearful tapestry that the Apostle John
weaves, there is a gleaming golden strand. <span class="hl">It seems that the Lord inspired John
to add this beautiful filament to provide hope, comfort, and direction for the
Saints living in the last days. Indeed, though one of the book’s significant
messages is a strong warning to the wicked, it also includes a divine promise
to the righteous.</span>” <small>(Richard Draper, Messages of the Book of Revelation for Latter-day Saints)</small>

<span class="na">&mdash;</span>

Peace and Refuge. These and more were John’s norm now, now that he had return-
ed from forced exile. His was staying at a place south of Ephesus, his chosen
home. It offered not a few comforts, many of which he had sorely been lacking.
Graciously, he soon forgot the suffering from before. And though he knew darker
clouds would gather over the horizon, TODAY he was at ease(!)&hellip;

Just then, John remembered the very beach on which he stood some months ago
prior. He felt and saw everything: <span class="hl">The waves crashing, the sun glistening — and
then it happened. A great beast emerged from the ocean, towering over him! And
though it was close and threatening, he wasn’t once harmed by it — yet here it
was, persistently hanging about him, rich in terrible detail.</span> The meaning was
troubling and unmistakable at the same time. He fumbled for his writing elem-
ents, only finding them after great effort. The cursive was oracular:

<span class="sd">“And I saw a beast rising out of the sea, having ten horns and seven heads &hellip;
upon his horns were ten diadems, and upon his heads was a slanderous name.”</span>

The BEAST of Revelation emerged before John’s mind in horrific detail. It look-
ed like a Leopard, yet had the feet of a Bear, and the mouth of a Lion(?!).
<span class="gb">‘What on earth does the Lord desire to portray to his Saints? This is a strange
convection indeed. Will they understand the symbology?’</span> As he pondered this,
the Spirit spoke peace to his mind, assuring him that they will understand &hellip; as
they apply themselves to understanding.

<span class="na">&mdash;</span>

<span class="sd">“One of his heads was as if it had been dealt a death wound, and [yet] his
death wound was healed[!]. And the whole world marveled at the beast. And they
[the world] worshipped the DRAGON, because he had given power to the beast &hellip;
‘Who is like the beast, and who can fight against him?’”</span> <small class="s1">(Revelation 13:1-4)</small>

John captured the imagery. There was more; much more in fact. But this was
enough for today. Funny that: The beast slithered back into the abyss, once
John captured the words exactly thus. Now as for their meaning? Well the rest
of it he would leave to God. <span class="gb">‘I trust the Saints will know the meaning accord-
ing to the spirit of revelation’</span> he thought as he scurried off to bed.

<span class="na">&mdash;</span>

Now for the tricky part, what to make of all this: The number ‘ten’ often rep-
resented (anciently) the whole of a part. <span class="hl">This is my interpretation mind, but
one could logically draw from this that the beast having ten horns and ten dia-
dems are a representation of many earthly kingdoms wielding great power &mdash; which
are really only a part, but appear to be the whole of it! In such manner, the
kingdoms of this world reign in false totality, and to many this is all there
is (completely ignoring the divine Kingdom of God)</span>.

“The numbers ‘seven’ and ‘ten’ to the pagan mind represent men and women work-
ing in harmony. Perhaps the Christian gentile reader of the first or second
century AD saw in the number ten the idea that the Adversary, throughout
earth’s history, was able to gain followers because he could mimic divine
rule (theocracy). Indeed, Satan’s rule was a substitute for divine rule.”

Finally, what are we to make of the slanderous name? Again, I borrow from
Draper &amp; colleague(s): “In the ancient world, names were more than labels or
markers of identification. They were viewed as an indispensable part of a
being’s personality. <span class="hl">To know the name of a god or demon was to have control
over it&hellip; The beast has a blasphemous name because such is his very essence.
He embodies blasphemy not only because he seeks to denigrate, revile, and
slander the Lord — but also because he claims powers that belong to God alone,
those of lawgiver and benefactor.</span>”

<span class="na">&mdash;</span>

“The DRAGON and the BEAST coexist! Thus, as the dragon is transhistorical, so
too is the beast. It represents not one demonic institution or power but ALL
institutions and powers across time.

“The numbers ‘seven’ and ‘ten’ thereby give three important if sinister nuances:
(1) The breadth and severity of the oppression on the faithful imposed by these
two monsters (2) The anti-Christian powers that promulgate and impose it, and
(3) The broad span of time in which these powers dominate.” <small>(New Testament Comment-
ary, The Revelation of John the Apostle)</small>

<span class="na">&mdash;</span>

That’s all for today’s fireside. <span class="gi">‘But what about the mark of the beast, and the
fire (lie) breathing dragon? I want to know more about the dragon Bilbo!’</span> Awe
yes, this and more will be ours as we continue the series of Times and Seasons.
Until then, I humbly bear my testimony of God’s truth as revealed through mod-
ern day prophets and apostles who hold the self-same authority given to the
ancients. <span class="hl">It is through the priesthood of God, bestowed upon Joseph Smith, Jr.
in this dispensation, and through the gift of the Holy Ghost, that we may know
the truth of all things; things which were, things which are, and of things yet
to come. In the divine name of Jesus Christ, amen.</span></span></pre></code></div>
</#macro>



<#macro part3>
<h1>My Beloved has Desired More</h1>

<p>As John is compiling his impressions, carefully transcribing his experiences that would
later comprise Revelation, he recalls a graceful moment between his Lord and his now late
friend Peter. He stops to reflect and emotions flow.
</p>

<p>While this recollection is a fun bit of fiction, the experience it touches upon is not.
During the month of April 1829 at Harmony, Pennsylvania, Joseph Smith was translating the Book
of Mormon, with his friend Oliver Cowdery writing, with little cessation, when the two of them
received several revelations. As he wrote, “A difference of opinion arising between us about the
<a href="https://www.josephsmithpapers.org/paper-summary/account-of-john-april-1829-c-dc-7/1">account of John the
Apostle</a>, mentioned in the New Testament, as to whether he died or continued to live, we mutually
agreed to settle it by the Urim and Thummim.”</p>

<p>That recorded revelation and more are part of this episode of Times and Seasons. It’s one of
my favorites and sets the stage for Part Four &mdash; which discusses the famous number and mark
of the beast.</p>

<div class="docs-example">
  <span>Times and Seasons &mdash; Part Three</span>
  <button class="m-1 btn btn-outline-primary btn-sm float-right" data-toggle="modal" data-target="#fireside-modal">
    <span class="icon icon-user"></span> Share This
  </button>
</div>

<div class="highlight"><pre><code class="language-html" data-lang="html">Saint John the DIVINE looked down upon the valley below. In the distance some
cattle moved about silently; neither their steps nor their lowing discernible.
He was staying with a friend south of Ephesus, where, just recently in fact, a
terrible fire had threatened the livelihoods of many. <span class="gb">‘What is there I can do
further?’</span> he thought as a gust of wind blew across his face, disarranging his
thoughts with ease. He returned inside.

<span class="hl">John loved the Saints. He knew many of them looked for the return of Christ as
if it would be this year or next. From what he had seen however, this was NOT
to be so. He loved his fellow man too (much), and today his heart yearned to
reach them, somehow.</span> And though he was among the Lord’s most trusted inner
circle, he was no ANGEL(?!). He had no trumpet to blast the majesty of creation
into the hearts of man. But most of all, John loved his Lord:

“And the Lord said unto me: <span class="gi">‘John, my beloved, what desirest thou? For if you
shall ask what you will, it shall be granted unto you.’</span> And I said unto him:
<span class="gb">‘Lord, give unto me POWER over death, that I may live and bring souls unto
thee.’</span> And the Lord said unto me: <span class="gi">‘Verily, verily, I say unto thee, because
thou desirest this thou shalt tarry until I come in my glory, and shalt
prophesy before nations, kindreds, tongues and people.’</span>” <small class="s1">(D&C 7:1-3)</small>

<span class="na">&mdash;</span>

<span class="gb">‘I miss Peter. I miss him.’</span> John was surprised to find tears already welling up
beneath his eyes. <span class="gb">‘I meant him no disrespect.’</span> He knew it, and his Lord knew it
as well. “<span class="gi">‘My beloved has desired that he might do more, or a greater work yet
among men than what he has before done. Yea, he has undertaken a greater work;
therefore I will make him as flaming fire and a ministering angel.’</span>” <small class="s1">(D&C 7:5-6)</small>

John knew it was time. He had resisted this for far too long. <span class="gb">‘I must at once
initiate the subject. It is time, it is right.’</span> As he prepared his mind for the
inevitable, and upon reaching for the writing elements, the vision BURST upon
his mind and she was there. SHE was there(!). The beauty and majesty of her
appearance overwhelmed the man:

<span class="sd">“And a great sign appeared in the sky, a WOMAN dressed in the sun, and the moon
was under her feet, and on her head was a crown of twelve stars. And the woman
was pregnant, and cried out in pain and agony of giving birth.”</span> <small class="s1">(Revelation 12:1-2)</small>

<span class="na">&mdash;</span>

Amen and good morning. Draper discusses the protagonist of Revelation with such
reverence. It is in this spirit that I wish to share with you the importance
of the woman in travail. <span class="hl">Before beasts and false prophets, and just before the
fire (lie) breathing DRAGON, there is a woman. She most assuredly represents
the Church of God, the Church of Christ. The WOMAN of Revelation is adored and
thus adorned in the most beautiful of garments: “Her garment is as the sun, a
brilliant radiance emanating from the Father and the Son that fully envelops
her. This heavenly glory represents both divine protection and deep purity,
a parity that guards her against corruption and defilement.”</span> <small>(Draper, New Test-
ament Commentary)</small>

Women to me(n) are nearly too much to comprehend. In the story, John can hardly
find the means within to write her part or role in the visions. HE struggles,
and it is only when the Lord nudges him, and when he ultimately reaches for his
writing elements, that he CAN and does. In like manner, to this man a woman’s
love, the incredibly important roles she plays, and the sublime LOVE she shows
and shares so naturally, so innately &mdash; well it astonishes me and we’ll leave it
at that. Thanks to you, and I wish for God’s love to be upon each and every one
of you; every woman in my life that has so loved and influenced me. You are &hellip;
astonishing(!).

<span class="na">&mdash;</span>

“In a number of places, the Old Testament portrays Israel as a woman in labor.
Isaiah uses the imagery to emphasize Israel’s helplessness before her enemies.
This seems to be the feeling that John wants to convey here. The woman is
vulnerable. Her pangs, however, suggest creative suffering, a suffering that
ends in joy as she brings forth a son.” <small>(New Testament Commentary, The Revelation of
John the Apostle)</small>

<span class="sd">“And another sign appeared in the sky, a great, fiery-red dragon that had seven
heads and ten horns, and on his heads were seven diadems. And his tail dragged
off a third of the stars of the sky, and he threw them to the earth(!).

“And the dragon was standing in front of the woman who was about to give birth,
to devour her child after it had been born. And she gave birth to a male child,
who was destined to shepherd all the nations with a rod of iron. But her CHILD
was taken away to GOD and to his throne.

“And the woman fled into the wilderness where she had a place prepared by God,
so that she could be taken care of there for one thousand two hundred sixty
days.”</span> <small class="si">(Revelation 12:3-6)</small> 

<span class="na">&mdash;</span>

The language is so so beautiful, is it not so? The threatened child is born and
saved from doom, the mother fleeing to her prepared place to await some further
call, seemingly. These excerpts I send out are translated from the earliest
Greek available, and I want to cry nearly every time I read from them. Surely,
many of the Bible’s plain and precious components have been robbed (taken) from
us!

As to their interpretation, I’m certain many have thoughts rolling around your
minds about now. If you would like to, I invite you to read again through the
above verses and compare them to the King James Version. Have a little fun with
it, again if you would like to do so. Perhaps use the following snippets from
Draper as a springboard to your study.

<u>Leviathan, sea monster, water serpent</u>: Ancient Israelites used these symbols as
rough synonyms for what we would consider Satan, that is, a supernatural oppo-
sitions to God’s order.

<u>Great, fiery-red dragon</u>: The Greek word drakōn is best understood as the
personification of malevolent and seething chaos &hellip; which opposes not only God
but all that is holy. It is no minor power. Indeed, in the OT it represents a
force that only God can subdue.

<span class="na">&mdash;</span>

“The distinguishing feature of the beast is its insatiable cruelty. It is
demonic in its genesis and intent, and as such, is the perfect type for Satan
at his worst. John sets this symbol against that of the woman, who represents
poise, harmony, beauty, and life-giving creation. In this context, the signif-
icance of the dragon’s color is heightened. It is fiery red, the color of that
which engulfs and consumes.

<span class="hl">“The power of the child is shown in the echo to Psalm 2:9, ‘he shall drive the
nations with an iron rod.’ That the dragon seeks to eliminate him while in the
very weak infant state suggests that the child is a greater threat to the
devil’s kingdom than the Church.</span> The text makes it clear that the woman was
successful in giving birth to the man child, even though, due to the power of
the adversary, he could not remain on the earth.” <small>(New Testament Commentary, The
Revelation of John the Apostle)</small>

<span class="hl">John loved the Saints. Upon recording his vision of the woman in travail, and
of the fiery-red dragon, words carefully chosen according to the SPIRIT within
him, the symbols disappear from view. One was terrible, the other unimaginably
beautiful. The GIFT of prophecy was his, and he sought to use it only in bene-
fit to his fellow man.</span> <span class="gb">‘I will trust the interpretation to them, as they apply
their minds to understanding,’</span> he thought as he returned outside. The cattle
had moved on passed his view.

I am grateful for the ministry of St. John the Divine; grateful for the gospel
as found in the Bible and in the Restoration Scriptures &mdash; to include the Book
of Mormon, another testament of Jesus Christ. Thank you for reading today. God
bless you(!). In the sacred name of Jesus Christ, amen.
</pre></code></div>
</#macro>
