
/*
 *   _  _                           _  
 *   )\/,) ___  ___  _ _  _    __   )) 
 *  ((`(( ((_( ((_( ((\( ((_( _))  ((  
 *               _))                o  
 *
 * Default dataset for magnus and other applications. This data is really meant
 * to bootstrap development and may (or may not) be useful for other things.
 *
 */



/*
 * BNP_ARTIST table (sample data)
 */
DELETE FROM BNP_ARTIST;

INSERT INTO BNP_ARTIST (ARTIST_ID, ARTIST_NAME, ARTIST_WEBSITE, YEARS_ACTIVE) VALUES
(1, 'Creedence Clearwater Revival', 'creedence-revisited.com', '1967-1972');
INSERT INTO BNP_ARTIST (ARTIST_ID, ARTIST_NAME, ARTIST_WEBSITE, YEARS_ACTIVE) VALUES
(2, 'George Harrison', 'www.georgeharrison.com', '1958-2001');
INSERT INTO BNP_ARTIST (ARTIST_ID, ARTIST_NAME, ARTIST_WEBSITE, YEARS_ACTIVE, WIKI_ARTICLE) VALUES
(3, 'O.M.D.', 'omd.uk.com', '1978–1996, 2006–present', 'https://en.wikipedia.org/wiki/Orchestral_Manoeuvres_in_the_Dark');
INSERT INTO BNP_ARTIST (ARTIST_ID, ARTIST_NAME, ARTIST_WEBSITE, YEARS_ACTIVE) VALUES
(4, 'Talk Talk', null, '1981-1992');
INSERT INTO BNP_ARTIST (ARTIST_ID, ARTIST_NAME, ARTIST_WEBSITE, YEARS_ACTIVE, WIKI_ARTICLE) VALUES
(5, 'The Smiths', 'www.officialsmiths.co.uk', '1982–1987', 'https://en.wikipedia.org/wiki/The_Smiths');
INSERT INTO BNP_ARTIST (ARTIST_ID, ARTIST_NAME, ARTIST_WEBSITE, YEARS_ACTIVE, WIKI_ARTICLE) VALUES
(6, 'Crowded House', 'www.officialstores.com.au/au_en/crowded-house', '1985–1996, 2006–present', 'https://en.wikipedia.org/wiki/Crowded_House');
INSERT INTO BNP_ARTIST (ARTIST_ID, ARTIST_NAME, ARTIST_WEBSITE, YEARS_ACTIVE, WIKI_ARTICLE) VALUES
(7, 'America', 'venturahighway.com', '1970-present', 'https://en.wikipedia.org/wiki/America_(band)');



/*
 * BNP_VINYL table
 */
DELETE FROM BNP_VINYL;

INSERT INTO BNP_VINYL (VINYL_ID, ARTIST_ID, RECORD_TITLE, YEAR_RELEASE) VALUES
(1, 2, 'Best of Dark Horse (1976-1989)', '1989');
INSERT INTO BNP_VINYL (VINYL_ID, ARTIST_ID, RECORD_TITLE, YEAR_RELEASE) VALUES
(2, 4, 'It''s My Life', '1984');
INSERT INTO BNP_VINYL (VINYL_ID, ARTIST_ID, RECORD_TITLE, YEAR_RELEASE, DISCOGS_LINK) VALUES
(3, 3, 'Orchestral Manoeuvres in the Dark', '1981', 'https://www.discogs.com/release/2311495');
INSERT INTO BNP_VINYL (VINYL_ID, ARTIST_ID, RECORD_TITLE, RECORD_LABEL, RECORD_NOTES, YEAR_RELEASE, YEAR_REISSUE, DISCOGS_LINK) VALUES
(4, 5, 'The Queen Is Dead', 'Rhino Records', 'I believe this is the 2012 reissue based upon the Catalog No.', '1984', '2012', 'https://www.discogs.com/The-Smiths-The-Queen-Is-Dead/release/3564656');
INSERT INTO BNP_VINYL (VINYL_ID, ARTIST_ID, RECORD_TITLE, RECORD_LABEL, RECORD_NOTES, YEAR_RELEASE, YEAR_REISSUE, DISCOGS_LINK) VALUES
(5, 6, 'Crowded House', 'Capitol Records', 'I love this album, absolutely love it, adore it and found it at Randy''s in fantastic shape in June ''19!', '1986', null, 'https://www.discogs.com/Crowded-House-Crowded-House/release/4706668');
INSERT INTO BNP_VINYL (VINYL_ID, ARTIST_ID, RECORD_TITLE, RECORD_LABEL, RECORD_NOTES, YEAR_RELEASE, YEAR_REISSUE, DISCOGS_LINK) VALUES
(6, 7, 'Homecoming', 'Warner Bros. Records', 'Found it one happy evening at Randy''s and I love nearly every track', '1972', '2009', 'https://www.discogs.com/America-Homecoming/release/12950308');



/*
 * BNP_TRACKS table
 */
DELETE FROM BNP_TRACKS;

INSERT INTO BNP_TRACKS (TRACK_ID, VINYL_ID, TRACK_NO, TRACK_TITLE) VALUES
(1, 3, 1, 'Enola Gay'), (2, 3, 2, '2nd Thought'), (3, 3, 3, 'Bunker Soldiers'), (4, 3, 4, 'Almost'), 
(5, 3, 5, 'Electricity'), (6, 3, 6, 'Statues'), (7, 3, 7, 'The Misunderstanding'), (8, 3, 8, 'Julia''s Song'),
(9, 3, 9, 'Motion and Heart'), (10, 3, 10, 'Messages'), (11, 3, 11, 'Stanlow');

INSERT INTO BNP_TRACKS (TRACK_ID, VINYL_ID, TRACK_NO, TRACK_TITLE, TRACK_NOTES) VALUES
(15, 4, 1, 'The Queen Is Dead', null), (16, 4, 2, 'Frankly, Mr. Shankly', null),
(17, 4, 3, 'I Know It''s Over', null), (18, 4, 4, 'Never Had No One Ever', null),
(19, 4, 5, 'Cemetry Gates', null), (20, 4, 6, 'Bigmouth Strikes Again', null),
(21, 4, 7, 'The Boy With The Thorn In His Side', null), (22, 4, 8, 'Vicar In A Train', null),
(23, 4, 9, 'There Is A Light That Never Goes Out', 'This is a fantastic song, and I heard it performed by Johnny Marr when he toured with Neil Finn back in 2002!'),
(24, 4, 10, 'Some Girls Are Bigger Than Others', null);

INSERT INTO BNP_TRACKS (TRACK_ID, VINYL_ID, TRACK_NO, TRACK_TITLE) VALUES
(25, 5, 1, 'World Where You Live'), (26, 5, 2, 'Now We''re Getting Somewhere'),
(27, 5, 3, 'Don''t Dream It''s Over'), (28, 5, 4, 'Mean To Me'),
(29, 5, 5, 'Love You ''Til The Day I Die'), (30, 5, 6, 'Something So Strong'),
(31, 5, 7, 'Hole In The River'), (32, 5, 8, 'I Walk Away'),
(33, 5, 9, 'Tombstone'), (34, 5, 10, 'That''s What I Call Love');

INSERT INTO BNP_TRACKS (TRACK_ID, VINYL_ID, TRACK_NO, TRACK_TITLE) VALUES
(35, 6, 1, 'Ventura Highway'), (36, 6, 2, 'To Each His Own'),
(37, 6, 3, 'Don''t Cross The River'), (38, 6, 4, 'Moon Song'),
(39, 6, 5, 'Only In Your Heart'), (40, 6, 6, 'Till The Sun Comes Up Again'),
(41, 6, 7, 'Cornwall Blank'), (42, 6, 8, 'Head And Heart'),
(43, 6, 9, 'California Revisited'), (44, 6, 10, 'Saturn Nights');


