
/*
 *   _  _                           _  
 *   )\/,) ___  ___  _ _  _    __   )) 
 *  ((`(( ((_( ((_( ((\( ((_( _))  ((  
 *               _))                o  
 *
 * Default dataset for magnus and other applications. This data is really meant
 * to bootstrap development and may (or may not) be useful for other things.
 *
 */


/*
 * KFR_PLAYER table
 */
DELETE FROM KFR_PLAYER;

ALTER TABLE KFR_PLAYER
CHANGE COLUMN DATE_LAST_REWARD DATE_LAST_REWARD TIMESTAMP NULL DEFAULT NULL,
CHANGE COLUMN DATE_CREATED DATE_CREATED TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
CHANGE COLUMN DATE_UPDATED DATE_UPDATED TIMESTAMP NULL DEFAULT NULL;

INSERT INTO KFR_PLAYER (PLAYER_ID, PLAYER_NAME) VALUES
(1, 'Theoderic the Great'),
(2, 'Gundobad of Burgandy'),
(3, 'Cholodomer the Arian');


/*
 * KFR_FIEFDOM table
 */
DELETE FROM KFR_FIEFDOM;

ALTER TABLE KFR_FIEFDOM
CHANGE COLUMN RECENT_ACTION RECENT_ACTION VARCHAR(32) NULL DEFAULT NULL,
CHANGE COLUMN DATE_CREATED DATE_CREATED TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
CHANGE COLUMN DATE_UPDATED DATE_UPDATED TIMESTAMP NULL DEFAULT NULL;

INSERT INTO KFR_FIEFDOM (FIEF_ID, PLAYER_ID) VALUES
(1, 1), (2, 2), (3, 3);


