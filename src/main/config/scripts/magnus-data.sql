  
/*
 *   _  _                           _  
 *   )\/,) ___  ___  _ _  _    __   )) 
 *  ((`(( ((_( ((_( ((\( ((_( _))  ((  
 *               _))                o  
 *
 * Default dataset for magnus and other applications. This data is really meant
 * to bootstrap development and may (or may not) be useful for other things.
 *
 */



/*
 * JVJ_ARTICLE table (sample data)
 */
DELETE FROM JVJ_ARTICLE;

INSERT INTO JVJ_ARTICLE (ARTICLE_ID, ARTICLE_TITLE, ARTICLE_URI, ACTIVE) VALUES
(1, 'King Floyd''s Empire', '/articles/floyd.html', 1);



/*
 * JVJ_FEATURE table (sample data)
 */
DELETE FROM JVJ_FEATURE;

INSERT INTO JVJ_FEATURE (FEATURE_ID, ARTICLE_ID, KEYWORDS, IMAGE_URL, MARKDOWN, ACTIVE, DATE_CREATED, DATE_MODIFIED) VALUES
(1, null, 'walkies', null, 'This is a super simple *feature* for display. Eventually this will have some images.', 1, '2019-07-14 16:50:24', '2019-07-14 16:50:24'),
(2, 1, 'floyd, king, king floyd, empire, games', '/_t/img/crown.jpg', 'Read all about **King Floyd** and the empire he helped to form.', 1, '2019-07-14 22:50:28', '2019-07-14 22:50:28');

INSERT INTO JVJ_FEATURE (FEATURE_ID, KEYWORDS, FEATURE_TITLE, FEATURE_URI, IMAGE_URL, MARKDOWN, POSITION, SITEWIDE, ACTIVE, DATE_CREATED, DATE_MODIFIED) VALUES
(3, 'tolkien, quote', 'Tolkien Quote', 'https://https://twitter.com/TolkienQuote', '/_t/img/avatar-tolk.jpg', 'goes here', 1, 1, 1, '2020-10-10 12:52:12', '2020-10-10 12:52:12');



/*
 * JVJ_PROFILE table (sample data)
 */
DELETE FROM JVJ_PROFILE;

INSERT INTO JVJ_PROFILE (PROFILE_ID, FIRST_NAME, LAST_NAME, DISPLAY_NAME, AVATAR_URL, BACKDROP_URL, PERSONAL_IDIOM, TWITTER_HANDLE, DATE_CREATED, DATE_MODIFIED) VALUES
(1, 'Justin', 'Peck', 'Justin Hobbendossen', '/_t/img/splash_3.jpg', '/_t/img/cliffs.jpg', 'Does this profile fit my idiom?', '@javajuster', '2019-07-14 17:08:44', '2019-07-14 17:08:44'),
(2, null, null, 'Ronaldus', '/download/statue.png', '/download/Aachener_Cathedral_Throne.jpg', 'I''ve noticed that everyone who is for abortion has already been born.', null, '2020-10-12 14:35:57', '2020-10-12 14:35:57');


