package us.javaj;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.jdbc.JdbcIndexedSessionRepository;
import org.springframework.session.jdbc.config.annotation.web.http.JdbcHttpSessionConfiguration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;
import org.springframework.util.StringUtils;


@Configuration
public class JdbcHttpSessionConfig {

  @Value("${clovis.session.table-name}")
  private String clovisSessionTable;
  
  @Value("${clovis.session.attribute.table-name}")
  private String clovisSessionAttributeTable;

  @Value("${clovis.session.timeout}") 
  private int clovisSessionTimeout;

  @Value("${clovis.session.cookie-name}")
  private String clovisCookieName;

  
  /**
   * Source class for Clovis database session repository configuration.
   */
  public class ClovisPlayerSessions extends JdbcHttpSessionConfiguration {

    @Bean
    @Override
    public JdbcIndexedSessionRepository sessionRepository() {
      JdbcIndexedSessionRepository sessionRepo = super.sessionRepository();
      sessionRepo.setDefaultMaxInactiveInterval(clovisSessionTimeout);
      sessionRepo.setTableName(clovisSessionTable);
      modifyAttributeQueries(sessionRepo);

      return sessionRepo;
    }

    @Bean
    public CookieSerializer cookieSerializer() {
      DefaultCookieSerializer serializer = new DefaultCookieSerializer();
      serializer.setDomainNamePattern("^.+?\\.(\\w+\\.[a-z]+)$");
      serializer.setCookiePath(DualSessionStrategy.CLOVIS);
      serializer.setCookieName(clovisCookieName); 
      serializer.setUseBase64Encoding(false);
      
      return serializer;
    }

    private void modifyAttributeQueries(JdbcIndexedSessionRepository sessionRepo) {
      sessionRepo.setGetSessionQuery(buildQuery(get));
      sessionRepo.setListSessionsByPrincipalNameQuery(buildQuery(list));
      sessionRepo.setCreateSessionAttributeQuery(buildQuery(create));
      sessionRepo.setUpdateSessionAttributeQuery(buildQuery(update));
      sessionRepo.setDeleteSessionAttributeQuery(buildQuery(delete));
    }

    private String buildQuery(String base) {
      String query = StringUtils.replace(base, "%SESSION_TABLE%", clovisSessionTable);
      return StringUtils.replace(query, "%ATTR_TABLE%", clovisSessionAttributeTable);
    }

    final String get    = "SELECT S.PRIMARY_ID, S.SESSION_ID, S.CREATION_TIME, S.LAST_ACCESS_TIME, S.MAX_INACTIVE_INTERVAL, "
                        + "SA.ATTRIBUTE_NAME, SA.ATTRIBUTE_BYTES FROM %SESSION_TABLE% S LEFT OUTER JOIN %ATTR_TABLE% SA "
                        + "ON S.PRIMARY_ID = SA.SESSION_PRIMARY_ID WHERE S.SESSION_ID = ?";
    
    final String list   = "SELECT S.PRIMARY_ID, S.SESSION_ID, S.CREATION_TIME, S.LAST_ACCESS_TIME, S.MAX_INACTIVE_INTERVAL, "
                        + "SA.ATTRIBUTE_NAME, SA.ATTRIBUTE_BYTES FROM %SESSION_TABLE% S LEFT OUTER JOIN %ATTR_TABLE% SA "
                        + "ON S.PRIMARY_ID = SA.SESSION_PRIMARY_ID WHERE S.PRINCIPAL_NAME = ?";

    final String create = "INSERT INTO %ATTR_TABLE%(SESSION_PRIMARY_ID, ATTRIBUTE_NAME, ATTRIBUTE_BYTES) "
                        + "SELECT PRIMARY_ID, ?, ? FROM %SESSION_TABLE% WHERE SESSION_ID = ?";

    final String update = "UPDATE %ATTR_TABLE% SET ATTRIBUTE_BYTES = ? WHERE SESSION_PRIMARY_ID = ? "
                        + "AND ATTRIBUTE_NAME = ?";

    final String delete = "DELETE FROM %ATTR_TABLE% WHERE SESSION_PRIMARY_ID = ? AND ATTRIBUTE_NAME = ?";
  }
}
