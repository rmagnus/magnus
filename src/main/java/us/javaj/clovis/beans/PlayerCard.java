package us.javaj.clovis.beans;

import javax.servlet.http.HttpSession;

import us.javaj.clovis.domain.Player;
import us.javaj.clovis.web.PlayerAction;
import us.javaj.clovis.web.event.RealmRundown;


@SuppressWarnings("serial")
public class PlayerCard extends GamePippin {
  public static final String PLAYER_DAT  = "playerData";
  public static final String PLAYER_ACT  = "playerAct";
  public static final String PLAYER_ID   = "player";

  private boolean beginningOfRound;

  public PlayerCard(Player player) {
    cachePlayerStats(player);
  }

  PlayerCard(Player player, HttpSession session) {
    beginningOfRound = true;
    buildSessionInfo(session);
    cachePlayerStats(player);
  }


  /**
   * Return the first round action when at the beginning of a round,
   * null otherwise. Associate this action with the player's session.
   *
   * @return firstAction
   */
  PlayerAction fetchFirstActionModel(HttpSession session) {
    if (beginningOfRound) {
      beginningOfRound = false;
      PlayerAction firstAction = new RealmRundown();
      data.put(PLAYER_ACT, firstAction);
      return firstAction;
    }

    return null;
  }


  /**
   * Return the next action based upon the current one, else return null.
   * The next action is what is now associated with the player's session.
   *
   * @return nextAction
   */
  PlayerAction advanceNextActionModel(HttpSession session) {
    PlayerAction currentAction = (PlayerAction)session.getAttribute(PLAYER_ACT);
    if (currentAction != null) {
      PlayerAction nextAction = currentAction.nextAction();
      data.put(PLAYER_ACT, nextAction);
      return nextAction;
    }

    return null;
  }
}
