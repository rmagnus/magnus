package us.javaj.clovis.beans;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import us.javaj.clovis.GameException;
import us.javaj.clovis.domain.Player;
import us.javaj.clovis.web.PlayerAction;


/**
 * A game registrar knows everything about how to register for a game,
 * authenticate individual players, the URI to which to send requests,
 * and more!
 */
@Component
public class Registrar {
  private static final Logger LOG = LoggerFactory.getLogger(Registrar.class);


  /**
   * Start up a new game for the passed player by associating the player's data
   * to their HTTP sessions. These game sessions are valid as long as a browser
   * remains open -and- the player makes a move within the allotted time limit.  
   *
   * @param player
   * @param session
   */
  public void startNewRound(Player player, HttpSession session) {
    PlayerData playerData = new PlayerCard(player, session);

    session.setAttribute(PlayerCard.PLAYER_ID, player.getId());
    session.setAttribute(PlayerCard.PLAYER_DAT, playerData);
    session.setAttribute(PlayerCard.PLAYER_ACT, null);

    LOG.debug("Starting new round for {} [ID:{}]", player.getPlayerUid(), player.getId());
  }


  /**
   * Call to return the current game in progress. The object returned contains
   * all the data needed to track a player through the various stages (rounds)
   * they have available to play.
   *
   * @param session
   * @param roundUnderway
   * @return GameBoard
   * @throws GameException
   */
  public PlayerCard getGameInProgress(HttpSession session, boolean roundUnderway)
    throws GameException {
    PlayerCard gameCard = (PlayerCard)session.getAttribute(PlayerCard.PLAYER_DAT);
    if (gameCard == null) {
      throw GameException.NO_GAME_IN_PROGRESS;
    }

    PlayerAction currentAction = (PlayerAction)session.getAttribute(PlayerCard.PLAYER_ACT);
    boolean startOfRoundButAction = currentAction != null && !roundUnderway;
    boolean roundUnderwayButNoAction = currentAction == null && roundUnderway;
    if (startOfRoundButAction || roundUnderwayButNoAction) {
      throw GameException.ILLEGAL_GAME_STATE;
    }

    return gameCard;
  }


  public PlayerAction getFirstEventOrAction(HttpSession session) throws GameException {
    PlayerCard gameCard = getGameInProgress(session, false);
    PlayerAction firstAction = gameCard.fetchFirstActionModel(session);
    if (firstAction != null) {
      session.setAttribute(PlayerCard.PLAYER_ACT, firstAction);
      session.setAttribute(PlayerCard.PLAYER_DAT, firstAction.applyPreAction(gameCard));

      return firstAction;    
    }

    throw GameException.ILLEGAL_GAME_STATE;
  }


  public PlayerAction getNextEventOrAction(HttpSession session) throws GameException {
    PlayerCard gameCard = getGameInProgress(session, true);
    PlayerAction nextAction = gameCard.advanceNextActionModel(session);
    if (nextAction != null) {
      session.setAttribute(PlayerCard.PLAYER_ACT, nextAction);
      session.setAttribute(PlayerCard.PLAYER_DAT, nextAction.applyPreAction(gameCard));

      return nextAction;
    }

    throw GameException.ILLEGAL_GAME_STATE;
  }


  public PlayerAction currentAction(String expectedAction, HttpSession session) throws GameException {
    PlayerCard gameCard = getGameInProgress(session, true);
    if (StringUtils.equals(expectedAction, gameCard.getActionName())) {
      return (PlayerAction)session.getAttribute(PlayerCard.PLAYER_ACT);
    }

    throw GameException.INVALID_ACTION;
  }


  /**
   * Determine if the presented action (model) is allow by checking it against the
   * action currently stored as game session attribute.
   *
   * @param session
   * @param model
   * @return boolean
   */
  public boolean isActionAllowed(PlayerAction action, HttpSession session) {
    boolean actionAllowed = false;
    try {
      String actionName = action.getClass().getSimpleName();
      PlayerAction currentAction = currentAction(actionName, session);
      boolean actionClassMatches = action.getClass().equals(currentAction.getClass());
      actionAllowed = actionClassMatches && isGameOnSessionValid(session); 
    }
    catch(GameException ex) {
      LOG.debug("Expected action does not match what was passed!", ex);
    }

    return actionAllowed;
  }


  /**
   * Inspect session to determine if an existing game is in progress.
   * Should be called prior to taking action on current game to avoid
   * game exceptions.
   *
   * @param session
   * @return boolean
   */
  private boolean isGameOnSessionValid(HttpSession session) {
    boolean gameOn = false;
    try {
      PlayerCard game = getGameInProgress(session, true);
      String gameDataPlayerId = Player.getPlayerId(game.getPlayerUid());
      String sessionAttributeId = String.valueOf(session.getAttribute(PlayerCard.PLAYER_ID));

      gameOn = StringUtils.equals(gameDataPlayerId, sessionAttributeId);
    }
    catch (IllegalStateException ex) {
      LOG.error("Session invalid", ex);
    }
    catch (GameException ex) {
      LOG.error("Game invalid", ex);
    }

    return gameOn;
  }
}
