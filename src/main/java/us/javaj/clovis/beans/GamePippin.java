package us.javaj.clovis.beans;

import us.javaj.clovis.GameException;


/**
 * The idea behind a Pippin is an early precedent to the Caroligian era
 * Mayor of the Palace, the one behind the king's court actually managing
 * the day to day affairs. As such, the Pippin in this case giveth and
 * taketh away according the the players actions and fortune.
 */
@SuppressWarnings("serial")
public abstract class GamePippin extends PlayerData {

  public PlayerData addAkers(final int amount) throws GameException {
    if (!adjustPlayerStat(FIEF_AKERS, amount)) {
      throw GameException.ADJUST_VALUE_FAILURE;
    }
    return this;
  }

  public PlayerData addPeons(final int amount) throws GameException {
    if (!adjustPlayerStat(FIEF_PEONS, amount)) {
      throw GameException.ADJUST_VALUE_FAILURE;
    }
    return this;
  }

  public PlayerData addBales(final int amount) throws GameException {
    if (!adjustPlayerStat(FIEF_BALES, amount)) {
      throw GameException.ADJUST_VALUE_FAILURE;
    }
    return this;
  }

  public PlayerData addSheep(final int amount) throws GameException {
    if (!adjustPlayerStat(FIEF_SHEEP, amount)) {
      throw GameException.ADJUST_VALUE_FAILURE;
    }
    return this;
  }

}
