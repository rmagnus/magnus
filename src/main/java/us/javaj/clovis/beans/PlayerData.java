package us.javaj.clovis.beans;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import us.javaj.clovis.GameException;
import us.javaj.clovis.KingConsole;
import us.javaj.clovis.domain.Player;


@SuppressWarnings("serial")
public abstract class PlayerData implements Serializable {

  /*
   * Contains all game data elements needed to progress through a round
   */
  protected Map<String, Object> data = new LinkedHashMap<>();

  protected static final String PLAYER_NAME     = "playerName";
  protected static final String PLAYER_UID      = "playerUid";

  protected static final String ROUNDS_PLAYED   = "roundsPlayed";
  protected static final String ROUNDS_LEFT     = "roundsLeft";

  public static final String FIEF_AKERS    = "akers";
  public static final String FIEF_PEONS    = "peons";
  public static final String FIEF_BALES    = "bales";
  public static final String FIEF_SHEEP    = "sheep";
  public static final String FIEF_HORSES   = "horses";
  public static final String FIEF_GEMS     = "gems";


  /**
   * Place some session data of interest along with game data stats. 
   *
   * @param player
   */
  protected void buildSessionInfo(HttpSession session) {
    data.put("sessionId", session.getId());
    data.put("creationTime", KingConsole.dateTime(session.getCreationTime()));
    data.put("accessedTime", KingConsole.dateTime(session.getLastAccessedTime()));
    data.put("expiresAfter", String.valueOf(session.getMaxInactiveInterval()) + " sec");
    data.put("sessionIsNew", String.valueOf(session.isNew()));
  }


  /**
   * Cache all key player stats as found from their collective fiefdom.
   *
   * @param player
   */
  protected void cachePlayerStats(Player player) {
    data.put("-", "-");
    data.put(PLAYER_NAME, player.getName());
    data.put(PLAYER_UID, player.getPlayerUid());
    data.put(ROUNDS_PLAYED, player.getRoundsPlayed());
    data.put(ROUNDS_LEFT, player.getRoundsLeft());

    data.put(FIEF_AKERS, player.getFiefdom().getAkers());
    data.put(FIEF_PEONS, player.getFiefdom().getPeons());
    data.put(FIEF_BALES, player.getFiefdom().getBales());
    data.put(FIEF_SHEEP, player.getFiefdom().getSheep());
    data.put(FIEF_HORSES, player.getFiefdom().getHorses());
    data.put(FIEF_GEMS, player.getFiefdom().getGems());
  }


  /**
   * Get the current card action being taken or null if none.
   *
   * @return simpleActionName
   */
  public String getActionName() {
    Object action = data.get(PlayerCard.PLAYER_ACT);
    return action != null ? action.getClass().getSimpleName() : null;
  }


  /**
   * Find the given game element and adjust it by the passed amount.
   *
   * @param element
   * @param amount
   * @return boolean
   */
  protected boolean adjustPlayerStat(String element, int amount) {
    boolean success = false;
    if (data.containsKey(element)) {
      Object value = data.get(element);
      try {
        int stat = Integer.parseInt(String.valueOf(value));
        data.put(element, (stat + amount));
        success = true;
      }
      catch (NumberFormatException ex) {
        success = false;
      }
    }
    return success;
  }


  /**
   * Returns the value associated with the passed game element associated
   * with this player. These values are updated according to player actions
   * taken and overall they are managed by the game's "Registrar".
   *
   * @param element
   * @return stat
   */
  public int getStat(String element) throws GameException {
    if (data.containsKey(element)) {
      return Integer.parseInt(data.get(element).toString());
    }
    throw GameException.NO_SUCH_GAME_ELEMENT;
  }

  public String getPlayerUid() {
    return (String)data.get(PLAYER_UID);
  }

  public String getPlayerName() {
    return (String)data.get(PLAYER_NAME);
  }

  /**
   * Utility method, use to display very basic game data information.
   *
   * @return session id, player uid, and player name
   */
  public String basicInfo() {
    final String[] keys = new String[] { "id", "playerName", "playerUid" };
    StringBuilder sb = new StringBuilder("|");
    data.entrySet().stream()
        .filter(entry -> entry.getKey() != null)
        .filter(entry -> Arrays.stream(keys).anyMatch(entry.getKey()::equals))
        .forEach(entry -> sb.append(entry.getValue()).append("|"));

    return sb.toString();
  }


  /**
   * For development purposes, build console data.
   *
   * @return html
   */
  public String consoleMarkup() {
    return KingConsole.consoleMarkup(data);
  }
}
