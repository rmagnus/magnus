package us.javaj.clovis.domain;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import us.javaj.data.UniqueEntity;


@Entity
@Table(name = "KFR_FIEFDOM")
@AttributeOverride(name="id", column=@Column(name="FIEF_ID"))
public class Fiefdom extends UniqueEntity {

  @Column(name = "AKERS")
  private int akers;

  @Column(name = "PEONS")
  private int peons;

  @Column(name = "BALES")
  private int bales;

  @Column(name = "SHEEP")
  private int sheep;

  @Column(name = "HORSES")
  private int horses;

  @Column(name = "GEMS")
  private int gems;

  @Column(name = "RECENT_ACTION")
  private String recentAction;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATE_UPDATED")
  private Date dateUpdated;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATE_CREATED")
  private Date dateCreated;

  @MapsId
  @OneToOne
  @JoinColumn(name = "FIEF_ID")
  private Player player;


  // Accessors
  public int getAkers() {
    return akers;
  }

  public void setAkers(int akers) {
    this.akers = akers;
  }

  public int getPeons() {
    return peons;
  }

  public void setPeons(int peons) {
    this.peons = peons;
  }

  public int getBales() {
    return bales;
  }

  public void setBales(int bales) {
    this.bales = bales;
  }

  public int getSheep() {
    return sheep;
  }

  public void setSheep(int sheep) {
    this.sheep = sheep;
  }

  public int getHorses() {
    return horses;
  }

  public void setHorses(int horses) {
    this.horses = horses;
  }

  public int getGems() {
    return gems;
  }

  public void setGems(int gems) {
    this.gems = gems;
  }

  public String getRecentAction() {
    return recentAction;
  }

  public void setRecentAction(String recentAction) {
    this.recentAction = recentAction;
  }

  public Date getDateUpdated() {
    return dateUpdated;
  }

  public void setDateUpdated(Date dateUpdated) {
    this.dateUpdated = dateUpdated;
  }

  public Date getDateCreated() {
    return dateCreated;
  }
}
