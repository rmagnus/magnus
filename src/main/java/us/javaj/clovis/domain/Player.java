package us.javaj.clovis.domain;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;

import us.javaj.data.UniqueEntity;


@Entity
@Table(name = "KFR_PLAYER")
@AttributeOverride(name="id", column = @Column(name="PLAYER_ID"))
public class Player extends UniqueEntity {
  public static final String UID_PREFIX  = "KFP-";

  @Column(name = "PLAYER_NAME", nullable = false)
  private String name;

  @Column(name = "ROUNDS")
  private int roundsLeft;

  @Column(name = "ROUNDS_PLAYED")
  private int roundsPlayed;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATE_LAST_REWARD")
  private Date dateLastReward;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATE_UPDATED")
  private Date dateUpdated;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "DATE_CREATED")
  private Date dateCreated;

  @OneToOne(mappedBy = "player", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
  private Fiefdom fiefdom;


  // Accessors
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRoundsLeft() {
    return roundsLeft;
  }

  public void setRoundsLeft(int roundsLeft) {
    this.roundsLeft = roundsLeft;
  }

  public int getRoundsPlayed() {
    return roundsPlayed;
  }

  public void setRoundsPlayed(int roundsPlayed) {
    this.roundsPlayed = roundsPlayed;
  }

  public Date getDateLastReward() {
    return dateLastReward;
  }

  public void setDateLastReward(Date dateLastReward) {
    this.dateLastReward = dateLastReward;
  }

  public Date getDateUpdated() {
    return dateUpdated;
  }

  public void setDateUpdated(Date dateUpdated) {
    this.dateUpdated = dateUpdated;
  }

  public Date getDateCreated() {
    return dateCreated;
  }

  public void setDateCreated(Date dateCreated) {
    this.dateCreated = dateCreated;
  }
  
  public Fiefdom getFiefdom() {
    return fiefdom;
  }
  
  public void setFiefdom(Fiefdom fiefdom) {
    this.fiefdom = fiefdom;
  }

  public String getPlayerUid() {
    return UID_PREFIX.concat(String.valueOf(getId()));
  }

  public static String getPlayerId(String uid) {
    return StringUtils.difference(Player.UID_PREFIX, uid);
  }
}
