package us.javaj.clovis;

import java.io.PrintStream;

@SuppressWarnings("serial")
public class GameException extends Exception {

  public static final GameException NO_GAME_IN_PROGRESS =
      new GameException("No game currently in progress!");

  public static final GameException ILLEGAL_GAME_STATE =
      new GameException("Game not in the correct state!");

  public static final GameException ADJUST_VALUE_FAILURE =
      new GameException("Could not adjust game value!");

  public static final GameException NO_SUCH_GAME_ELEMENT =
      new GameException("No such game data element!");

  public static final GameException INVALID_ACTION =
      new GameException("Action not allowed or no session!");

  public static final GameException INVALID_PLAYER_AUTH =
      new GameException("Not a valid auth attempt!");


  /**
   * Constructs a game exception for whatever reason.
   *
   * @param message
   */
  public GameException(String message) {
    super(message);
  }


  public void toString(PrintStream out) {
    StringBuilder sb = new StringBuilder();
    sb.append("\n ______________________________\n");
    sb.append("\n ").append(getMessage());
    sb.append("\n ______________________________\n");
    out.append(sb.append("\n\n").toString());
  }
}
