package us.javaj.clovis;

import java.util.Random;


public class EightSidedDie {

  public static final int roll() {
    return new Random().nextInt(8) + 1;
  }
}
