package us.javaj.clovis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import us.javaj.clovis.domain.Player;


@Repository
public interface PlayerRepo extends JpaRepository<Player, Integer> {

}
