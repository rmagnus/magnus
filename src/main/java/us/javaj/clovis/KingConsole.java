package us.javaj.clovis;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Map.Entry;

import us.javaj.clovis.web.PlayerAction;

public class KingConsole {
  private static final String ID   = "sessionId";
  private static final String ACT  = "playerAct";
  private static final String DEL  = ":-&raquo; ";
  private static final String BR   = "<br>";
  private static final String PS   = "<p>";
  private static final String PE   = "</p>";


  /**
   * For game development purposes really.
   *
   * @return HTML markup
   */
  public static String consoleMarkup(Map<String, Object> data) {
    StringBuilder sb = new StringBuilder();
    data.entrySet().stream().forEach(
        entry -> sb.append(PS).append(nameValueFor(entry)).append(PE));

    return sb.toString();
  }


  private static String nameValueFor(Entry<String, Object> e) {
    if ("-".equals(e.getKey())) { // console header
      return BR + PS + "Player Stats [PLAYER_DATA]" + PE;
    }
    else if (ID.equals(e.getKey())) { // session info
      return PS + "Clovis Session [SESSION_ID]" + PE + PS + e.getValue() + PE;
    }
    else if (ACT.equals(e.getKey())) { // action info
      PlayerAction action = (PlayerAction)e.getValue();
      String value = action.getClass().getSimpleName() + DEL + action.getView();
      return BR + PS + "Player Action [PLAYER_ACT]" + PE + PS + value + PE;
    }
    else { // individual stats that make up a kingdom
      return e.getKey().concat(": ").concat(String.valueOf(e.getValue()));
    }
  }


  public static String dateTime(long millis) {
    ZonedDateTime dateTime = Instant.ofEpochMilli(millis).atZone(ZoneId.systemDefault());
    return dateTime.format(DateTimeFormatter.ofPattern("MM/dd h:mm:ss a"));
  }
}
