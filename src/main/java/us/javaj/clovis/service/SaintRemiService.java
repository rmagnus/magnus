package us.javaj.clovis.service;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;
import us.javaj.clovis.domain.Player;
import us.javaj.clovis.repo.PlayerRepo;
import us.javaj.clovis.web.PlayerAction;


/**
 * A former Arian Christian, Clovis eventually converted to Catholicism following
 * the Battle of Tolbiac on Christmas Day 508 in a small church in the vicinity
 * of the subsequent Abbey of Saint-Remi in Reims.
 *
 * <p>This service will "bless" the various actions the player takes by persisting
 * whatever gains or loses to his or her fief. The model passed to whatever service
 * method herein should be approved first by the game Registrar.  
 */
@Service
public class SaintRemiService {
  private static final Logger LOG = LoggerFactory.getLogger(SaintRemiService.class);

  @Autowired
  private PlayerRepo playerRepo;


  /**
   * Take the given model and game (board) data and record it to the database.
   * The model returned will be specific to the next applicable action a player
   * may take.
   *
   * @param game
   * @param model
   * @return model
   */
  @Transactional
  public void chroniclePlayerAction(PlayerCard gameCard, PlayerAction action)
      throws GameException {
    String playerId = Player.getPlayerId(gameCard.getPlayerUid());
    Player player = playerRepo.getOne(Integer.valueOf(playerId));
    playerRepo.save(action.reflectChanges(gameCard, player));

    LOG.debug("Saint Remi has chronicled player action {} ", action);
  }
}
