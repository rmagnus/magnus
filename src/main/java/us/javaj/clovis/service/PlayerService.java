package us.javaj.clovis.service;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import us.javaj.clovis.GameException;
import us.javaj.clovis.domain.Player;
import us.javaj.clovis.repo.PlayerRepo;


@Service
public class PlayerService {
  private static final Log LOG = LogFactory.getLog(PlayerService.class);

  @Autowired
  private PlayerRepo playerRepo;


  public List<Player> retrieveAllPlayers() {
    return playerRepo.findAll();
  }


  public Player retrievePlayer(Integer id) {
    return playerRepo.findById(id).get();
  }


  public Player fauxAuthentication(String uid) throws GameException {
    if (isValidPlayerUid(uid)) {
      return retrievePlayer(Integer.valueOf(Player.getPlayerId(uid)));
    }

    throw GameException.INVALID_PLAYER_AUTH;
  }


  private boolean isValidPlayerUid(String uid) {
    boolean validPlayerUid = false;
    try {
      validPlayerUid = playerRepo.existsById(Integer.valueOf(Player.getPlayerId(uid)));
    }
    catch (NumberFormatException ex) {
      LOG.error("Invalid uid", ex);
    }
    catch (IllegalArgumentException ex) {
      LOG.error("Invalid uid", ex);
    }

    return validPlayerUid;
  }
}
