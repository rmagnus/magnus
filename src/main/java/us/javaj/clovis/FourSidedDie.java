package us.javaj.clovis;

import java.util.Random;


public class FourSidedDie {

  public static final int roll() {
    return new Random().nextInt(4) + 1;
  }
}
