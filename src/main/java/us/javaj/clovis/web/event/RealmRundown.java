package us.javaj.clovis.web.event;

import java.io.Serializable;

import us.javaj.clovis.web.PlayerAction;
import us.javaj.clovis.web.BaseController;


@SuppressWarnings("serial")
public class RealmRundown implements PlayerAction, Serializable {

  @Override
  public String getView() {
    return REALM_VIEW;
  }

  @Override
  public String getEndpoint() {
    return BaseController.REALM_ENDPOINT;
  }

  @Override
  public PlayerAction nextAction() {
    return new PeeonHarvest();
  }
}
