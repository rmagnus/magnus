package us.javaj.clovis.web.event;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import us.javaj.clovis.GameException;
import us.javaj.clovis.web.BaseController;


/**
 * Basically a quick rundown of the current status of a player's kingdom (realm).
 */
@Controller
public class RealmRundownController extends BaseController {

  @GetMapping(REALM_ENDPOINT)
  public ModelAndView showView(HttpSession session) throws GameException {
    return getActionModel("RealmRundown", session);
  }


  @PostMapping(REALM_ENDPOINT)
  public View bindAndValidate(HttpSession session, RealmRundown model, BindingResult result)
    throws GameException {
    return persistActionReturnNext(model, session);
  }
}
