package us.javaj.clovis.web.event;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.javaj.clovis.EightSidedDie;
import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;
import us.javaj.clovis.beans.PlayerData;
import us.javaj.clovis.domain.Fiefdom;
import us.javaj.clovis.domain.Player;
import us.javaj.clovis.web.BaseController;
import us.javaj.clovis.web.PlayerAction;
import us.javaj.clovis.web.market.GiftSheep;


@SuppressWarnings("serial")
public class PeeonHarvest implements PlayerAction, Serializable {
  private static final Logger LOG = LoggerFactory.getLogger(PeeonHarvest.class);

  /*
   * Bales that are harvested based upon player land, workers, and providence
   */
  private int bales;

  private String option1;
  private String option2;
  private String option3;
  private String option4;

  private double choice;

  @Override
  public String getView() {
    return HARVEST_VIEW;
  }

  @Override
  public String getEndpoint() {
    return BaseController.HARVEST_ENDPOINT;
  }

  @Override
  public PlayerAction nextAction() {
    return new GiftSheep();
  }


  /**
   * According the number of peeons and land available to work, a harvest is
   * calculated with a die roll that will affect the amount grown by a random
   * multiple. For now, just granting the player 1000 bales of wheat.
   *
   * @param gameCard
   * @return playerData
   */
  @Override
  public PlayerData applyPreAction(PlayerCard gameCard) throws GameException {
    bales = calculateHarvest(gameCard);
    buildDistributionOptions(gameCard);

    return gameCard.addBales(bales);
  }


  /**
   * Needs to be rewritten to reflect the actual code from KFE..!
   *
   * @param gameCard
   * @return playerAction
   */
  @Override
  public PlayerAction effectuateResult(PlayerCard gameCard) throws GameException {
    LOG.info("Player has opted to feed ratio {}", choice);

    int peons = gameCard.getStat(PlayerData.FIEF_PEONS);
    double expendedBales = peons * choice;

    int populationDiff = 0;
    int option = (int) choice * 2;
    switch(option) {

    case 0:
      // lose half
      populationDiff = (peons / 2) * -1;
      break;
      
    case 1:
      // lose quarter
      populationDiff = (peons / 4) * -1;
      break;

    case 2:
      // add ten percent
      double tenPercentIncrease = (double)peons * 0.1;
      populationDiff = (int)tenPercentIncrease;
      break;

    case 4:
      // add twenty percent
      double populationIncrease = (double)peons * 0.2;
      populationDiff = (int)populationIncrease;
      break;
    }

    int negatedBales = (int)expendedBales * -1;
    LOG.info("Player harvest population difference {}", populationDiff);
    LOG.info("Player bales expended {}", negatedBales);
    gameCard.addPeons(populationDiff);
    gameCard.addBales(negatedBales);

    return this;
  }


  @Override
  public Player reflectChanges(PlayerCard gameCard, Player player) throws GameException {
    Fiefdom playerRealm = player.getFiefdom();
    playerRealm.setPeons(gameCard.getStat(PlayerCard.FIEF_PEONS));
    playerRealm.setBales(gameCard.getStat(PlayerCard.FIEF_BALES));
    return player;
  }


  /**
   * This method will calculate and return the number of new balees grown
   * for the given season slash game round.
   *
   * @param gameCard
   * @return balesGrown
   * @throws GameException
   */
  private int calculateHarvest(PlayerCard gameCard) throws GameException {
    int roll = EightSidedDie.roll();
    LOG.info("Player has rolled a [{}] harvest roll (8-sided)", roll);

    // akers worked based on availability and peeons
    int akers = gameCard.getStat(PlayerData.FIEF_AKERS);
    int peons = gameCard.getStat(PlayerData.FIEF_PEONS);
    int storage = gameCard.getStat(PlayerData.FIEF_BALES);
    LOG.info("Player has {} peons to work {} akers", peons, akers);

    int peonsWorking = peons / 2; // it takes two peons to work one aker
    if (peonsWorking > akers) {
      peonsWorking = akers;
      LOG.info("Peons adjusted to {} due to too few akers", peonsWorking);
    }
    
    int percentage = 10 / 10; // used later for percentage working field
    int dimReturns = calculateDiminishingReturnsFactor(akers);
    int harvest = ((peonsWorking * percentage) + 1) / dimReturns;
    /*
    if (harvest > akers) {
      harvest = akers;
    }
    */
    LOG.info("Player harvest PRIOR to roll is {} bales", harvest);

    int balesGrown = harvest * roll;
    int total = storage + balesGrown;
    LOG.info("Player has grown {} bales with {} total", balesGrown, total);

    return balesGrown;
  }


  /**
   * Diminshing returns is the notion that the more land a player has the less
   * it yeilds per aker. The law shows that beyond a certain yield, the rate of
   * increase in yields decreases, such that the additional input cost is not
   * compensated by additional yields.
   *
   * @param akers
   * @return factor
   */
  private int calculateDiminishingReturnsFactor(int akers) {
    int dimReturns = 1;

    if (akers <= 10000) {
      dimReturns = 2;
    }
    else if (akers > 10000) {
      dimReturns = 3;
    }
    else if (akers > 20000) {
      dimReturns = 4;
    }
    else if (akers > 30000) {
      dimReturns = 5;
    }
    else if (akers > 40000) {
      dimReturns = 6;
    }
    else if (akers > 50000) {
      dimReturns = 7;
    }
    else if (akers > 60000) {
      dimReturns = 8;
    }
    else if (akers > 80000) {
      dimReturns = 9;
    }
    else if (akers > 100000) {
      dimReturns = 10;
    }
    else if (akers > 125000) {
      dimReturns = 11;
    }
    else if (akers > 150000) {
      dimReturns = 12;
    }
    LOG.info("Diminishing returns factor is {}", dimReturns);

    return dimReturns;
  }


  private void buildDistributionOptions(PlayerCard gameCard) throws GameException {
    int peons = gameCard.getStat(PlayerData.FIEF_PEONS);
    int storage = gameCard.getStat(PlayerData.FIEF_BALES);
    int totalBales = storage + getBales();

    option1 = "Do Not Ration Any Bales This Season";
    if (totalBales >= (peons / 2)) {
      option2 = "Distribute Half Rations -or- ? Bales".replace("?", String.valueOf(peons / 2));
    }
    if (totalBales >= peons) {
      option3 = "Distribute Full Rations -or- ? Bales".replace("?", String.valueOf(peons));
    }
    if (totalBales >= (peons * 2)) {
      option4 = "Feed Double Rations -or- ? Bales".replace("?", String.valueOf(peons * 2));
    }
  }


  // Accessors
  public int getBales() {
    return bales;
  }

  public String getOption1() {
    return option1;
  }

  public String getOption2() {
    return option2;
  }

  public String getOption3() {
    return option3;
  }

  public String getOption4() {
    return option4;
  }

  public boolean isOption1Available() {
    return option1 != null;
  }

  public boolean isOption2Available() {
    return option2 != null;
  }

  public boolean isOption3Available() {
    return option3 != null;
  }

  public boolean isOption4Available() {
    return option4 != null;
  }

  public double getChoice() {
    return choice;
  }

  public void setChoice(double choice) {
    this.choice = choice;
  }
}
