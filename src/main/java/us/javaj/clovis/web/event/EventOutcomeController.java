package us.javaj.clovis.web.event;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import us.javaj.clovis.GameException;
import us.javaj.clovis.web.BaseController;


/**
 * Generate and display the outcome of a random event picked from
 * any number of events, which can grow or shrink over time.
 */
@Controller
public class EventOutcomeController extends BaseController {

  @GetMapping(EVENT_ENDPOINT)
  public ModelAndView showView(HttpSession session) throws GameException {
    return getActionModel("EventOutcome", session);
  }


  @PostMapping(EVENT_ENDPOINT)
  public View bindAndValidate(HttpSession session, EventOutcome model, BindingResult result)
    throws GameException {
    return persistActionReturnNext(model, session);
  }
}
