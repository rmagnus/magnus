package us.javaj.clovis.web.event;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;
import us.javaj.clovis.web.BaseController;


/**
 * Tell the player what kind of harvest they have had, and allow them to choose
 * what amount to feed their people. Feeding more will help the population expand,
 * and feeding less with cause disease and famine, etc.
 */
@Controller
public class PeeonHarvestController extends BaseController {

  @GetMapping(HARVEST_ENDPOINT)
  public ModelAndView showView(HttpSession session) throws GameException {
    return getActionModel("PeeonHarvest", session);
  }


  @PostMapping(HARVEST_ENDPOINT)
  public View bindAndValidate(HttpSession session, PeeonHarvest model, BindingResult result)
    throws GameException {
    PlayerCard gameCard = registrar.getGameInProgress(session, true);

    return persistActionReturnNext(model.effectuateResult(gameCard), session);
  }
}
