package us.javaj.clovis.web;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;


/**
 * The official traffic cop for all client requests made of the /clovis endpoint.
 * GET requests will simply display the current game template (view) according
 * to a player's progress through the round. POST requests assume the player is
 * taking a specific action from a previously displayed template and will then
 * call the appropriate service method in order to process the player's action
 * and determine the action's result.
 */
@Controller
public class ClovisController extends BaseController {
  protected static final Logger LOG = LoggerFactory.getLogger(ClovisController.class);

  /**
   * Determine if there is a game in progress, and if so, direct to the view necessary
   * to start a new game. After this endpoint has been requested or refreshed, it will
   * be unavailable as each game controller must process user actions via its endpoint.
   *
   * @param session
   * @return ModelAndView
   * @throws GameException
   */
  @GetMapping(CLOVIS_ENDPOINT)
  public ModelAndView displayNewRound(HttpSession session) throws GameException {
    PlayerCard player = registrar.getGameInProgress(session, false);
    LOG.debug("Display new round for {}", player.basicInfo());

    return new ModelAndView(PlayerAction.INITIAL_VIEW, "clovis", player);
  }


  /**
   * Consult the game Registrar for the first even to option the play has when
   * starting a new round of the game. Throws an exception if this is not the
   * correct state of the round.
   *
   * @param session
   * @return RedirectView
   * @throws GameException
   */
  @PostMapping(CLOVIS_ENDPOINT)
  public RedirectView returnFirstEventOrAction(HttpSession session) throws GameException {
    PlayerAction action = registrar.getFirstEventOrAction(session);
    LOG.debug("Return first player action view {}", action.getView());

    return new RedirectView(action.getEndpoint());
  }
}
