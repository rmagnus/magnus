package us.javaj.clovis.web;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;
import us.javaj.clovis.beans.Registrar;
import us.javaj.clovis.service.SaintRemiService;


public abstract class BaseController {

  public static final String CLOVIS_ENDPOINT     = "/clovis";
  public static final String REALM_ENDPOINT      = "/clovis/rundown";
  public static final String EVENT_ENDPOINT      = "/clovis/event";
  public static final String HARVEST_ENDPOINT    = "/clovis/harvest";
  public static final String GIFT_ENDPOINT       = "/clovis/gift";
  public static final String STAT_ENDPOINT       = "/clovis/advance";

  @Autowired
  protected Registrar registrar;

  @Autowired
  protected SaintRemiService saintRemi;


  /**
   * Each game controller should call this to verify and return the expected
   * model for display. Doing so will ensure the player cannot skip around
   * from one action to another in some haphazard way (for example cheat). 
   *
   * @param expectedModel
   * @param session
   */
  protected final ModelAndView getActionModel(String expectedAction, HttpSession session)
      throws GameException {
    /*
     * Game actions are accessed in the view as action.property
     */
    PlayerAction model = registrar.currentAction(expectedAction, session);
    return new ModelAndView(model.getView(), "action", model);
  }


  /**
   * Determine what action was taken by the player, and if allowed chronicle (persist)
   * and reflect the action to the game board using the game Registrar. The Saint Remi
   * service persists changes to the database while the Registrar informs the next view
   * to display. Keeping these two functions separate will help ensure a player cannot
   * hack game data by fiddling with a client browser.
   *
   * @param action
   * @param session
   * @return redirectView
   * @throws GameException
   */
  protected RedirectView persistActionReturnNext(PlayerAction action, HttpSession session)
      throws GameException {
    if (registrar.isActionAllowed(action, session)) {
      PlayerCard gameCard = registrar.getGameInProgress(session, true);
      saintRemi.chroniclePlayerAction(gameCard, action);

      return new RedirectView(registrar.getNextEventOrAction(session).getEndpoint());
    }

    throw GameException.INVALID_ACTION;
  }


  /**
   * Display any gaming exception that may occur. 
   *
   * @param gameException
   * @return modelAndView
   */
  @ExceptionHandler(GameException.class)
  public ModelAndView handleGameExceptions(GameException gameException) {
    gameException.toString(System.err);

    return new ModelAndView("/clovis/error", "error", gameException);
  }
}
