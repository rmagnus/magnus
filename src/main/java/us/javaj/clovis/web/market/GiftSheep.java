package us.javaj.clovis.web.market;

import java.io.Serializable;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;
import us.javaj.clovis.beans.PlayerData;
import us.javaj.clovis.domain.Fiefdom;
import us.javaj.clovis.domain.Player;
import us.javaj.clovis.web.BaseController;
import us.javaj.clovis.web.PlayerAction;


/**
 * Simply gift of a random number of sheep from the king.
 */
@SuppressWarnings("serial")
public class GiftSheep implements PlayerAction, Serializable {

  @Override
  public String getView() {
    return GIFT_VIEW;
  }

  @Override
  public String getEndpoint() {
    return BaseController.GIFT_ENDPOINT;
  }

  @Override
  public PlayerAction nextAction() {
    return new TempStatus();
  }


  /**
   * Simply gift the player a sheep (session).
   */
  @Override
  public PlayerData applyPreAction(PlayerCard gameBoard) throws GameException {
    return gameBoard.addSheep(1);
  }


  /**
   * Player has accepted the gift (database).
   */
  @Override
  public Player reflectChanges(PlayerCard gameBoard, Player player) throws GameException {
    Fiefdom playerKingdom = player.getFiefdom();
    playerKingdom.setSheep(gameBoard.getStat(PlayerCard.FIEF_SHEEP));
    return player;
  }
}
