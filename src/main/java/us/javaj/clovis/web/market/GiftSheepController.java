package us.javaj.clovis.web.market;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import us.javaj.clovis.GameException;
import us.javaj.clovis.web.BaseController;


/**
 * Sample "player action" controller which will reward the player with a random
 * number of sheep for free!
 */
@Controller
public class GiftSheepController extends BaseController {

  @GetMapping(GIFT_ENDPOINT)
  public ModelAndView showView(HttpSession session) throws GameException {
    return getActionModel("GiftSheep", session);
  }


  @PostMapping(GIFT_ENDPOINT)
  public View bindAndValidate(HttpSession session, GiftSheep model, BindingResult result)
    throws GameException {
    return persistActionReturnNext(model, session);
  }
}
