package us.javaj.clovis.web.market;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import us.javaj.clovis.GameException;
import us.javaj.clovis.web.BaseController;


/**
 * Sample "player action" controller which will reward the player with a random
 * number of sheep for free!
 */
@Controller
public class TempStatusController extends BaseController {

  @GetMapping(STAT_ENDPOINT)
  public ModelAndView showView(HttpSession session) throws GameException {
    return getActionModel("TempStatus", session);
  }
}
