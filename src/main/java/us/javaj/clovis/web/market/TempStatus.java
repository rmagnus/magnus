package us.javaj.clovis.web.market;

import java.io.Serializable;

import us.javaj.clovis.web.BaseController;
import us.javaj.clovis.web.PlayerAction;


@SuppressWarnings("serial")
public class TempStatus implements PlayerAction, Serializable {

  @Override
  public String getView() {
    return STATUS_VIEW;
  }

  @Override
  public String getEndpoint() {
    return BaseController.STAT_ENDPOINT;
  }

  @Override
  public PlayerAction nextAction() {
    return null;
  }
}
