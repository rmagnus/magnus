package us.javaj.clovis.web;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;
import us.javaj.clovis.beans.PlayerData;
import us.javaj.clovis.domain.Player;


/**
 * Class to implement that represents a form command or better yet the action
 * the player takes at any particular stage of the round. For example, if the
 * play purchases fifty sheep at the market, this must be reflected to both
 * the game board (HTTP session data) as well as the player's datastore.  
 */
public interface PlayerAction {
  static final String INITIAL_VIEW    = "clovis/start";
  static final String ERROR_VIEW      = "clovis/error";
  static final String REALM_VIEW      = "clovis/event/realm";
  static final String EVENT_VIEW      = "clovis/event/outcome";
  static final String HARVEST_VIEW    = "clovis/event/harvest";
  static final String GIFT_VIEW       = "clovis/gift";
  static final String STATUS_VIEW     = "clovis/advance";

  /**
   * Not required, but perform any pre-action to set the stage for the
   * decision a player will make or action the player is about to take.
   *
   * @param gameCard
   * @return PlayerData
   */
  default PlayerData applyPreAction(PlayerCard gameCard) throws GameException {
    return gameCard;
  }

  /**
   * Each player action or choice will likely have some effect on their
   * realm overall stats. Override with appropriate calculations. 
   *
   * @param gameCard
   * @return PlayerData
   */
  default PlayerAction effectuateResult(PlayerCard gameCard) throws GameException {
    return this;
  }

  /**
   * Whatever action a player takes, the result should be reflected to both
   * the player card data (session) and to the player's database stats. These
   * changes to a player's stats should be persisted in the event the session
   * is dropped or another player lays claim upon some of their resources.
   *
   * @param gameBoard
   * @param player
   * @return Player
   */
  default Player reflectChanges(PlayerCard gameCard, Player player) throws GameException {
    return player;
  }

  /**
   * Each model must specify its own view (template).
   *
   * @return view
   */
  public String getView();

  /**
   * Simple way to bind controllers to their model "actions".
   *
   * @return endpoint
   */
  public String getEndpoint();

  /**
   * Each model must specify the model which succeeds it.
   *
   * @return GameActionModel
   */
  public PlayerAction nextAction();
}
