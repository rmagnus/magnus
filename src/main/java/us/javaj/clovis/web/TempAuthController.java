package us.javaj.clovis.web;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.Registrar;
import us.javaj.clovis.service.PlayerService;


@Controller
@RequestMapping("/clovis/players")
public class TempAuthController {
  private static final Logger LOG = LoggerFactory.getLogger(TempAuthController.class);

  @Autowired
  private Registrar registrar;

  @Autowired
  PlayerService playerService;


  @GetMapping
  public void showRegisteredPlayers(Model model) {
    model.addAttribute("players", playerService.retrieveAllPlayers());
  }


  @PostMapping
  public RedirectView fauxAuthentication(@RequestParam("uid") String uid, HttpSession session)
      throws GameException {
    registrar.startNewRound(playerService.fauxAuthentication(uid), session);
    LOG.debug("Faux authentication successful for {}", uid);

    return new RedirectView(BaseController.CLOVIS_ENDPOINT);
  }
}
