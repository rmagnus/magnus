package us.javaj;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
public class JavaDataPersistence {
  private static final Log LOG = LogFactory.getLog(JavaDataPersistence.class);

  private static String banPrefix = "pkg_";
  private static String banSuffix = ".txt";

  @Value("${spring.datasource.driver-class-name}")
  private String driverClassName;

  @Value("${spring.datasource.url}")
  private String datasourceUrl;

  @Value("${spring.datasource.username}")
  private String datasourceUsername;

  @Value("${spring.datasource.password}")
  private String datasourcePassword;

  @Value("${spring.jpa.show-sql}")
  private boolean showGeneratedSql;

  @Value("${magnus.persistence.packages}")
  private String[] packagesToScan;


  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    List.of(packagesToScan).forEach(name -> displayPackageToScan(name));
    em.setPackagesToScan(packagesToScan);
    em.setDataSource(dataSource());

    JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    em.setJpaProperties(specificToHibernate());
    em.setJpaVendorAdapter(vendorAdapter);

    return em;
  }


  @Bean
  public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(emf);

    return transactionManager;
  }


  @Bean
  public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
    return new PersistenceExceptionTranslationPostProcessor();
  }


  @Bean
  public DataSource dataSource(){
     DriverManagerDataSource dataSource = new DriverManagerDataSource();
     dataSource.setDriverClassName(driverClassName);
     dataSource.setUrl(datasourceUrl);
     dataSource.setUsername(datasourceUsername);
     dataSource.setPassword(datasourcePassword);
     return dataSource;
  }


  void displayPackageToScan(String pkg) {
    String file = banPrefix.concat(pkg.substring(pkg.lastIndexOf(".") + 1).concat(banSuffix));
    Resource res = new ClassPathResource(file);
    try {
      String banner = String.format("Scanning [%s] persistence package\n", pkg);
      LOG.info(banner.concat(new String(Files.readAllBytes(res.getFile().toPath()))));
    }
    catch (IOException e) {
      LOG.warn(String.format("No banner for unheralded [%s] package", pkg));
    }
  }


  Properties specificToHibernate() {
    Properties properties = new Properties();
    properties.setProperty("hibernate.hbm2ddl.auto", StringUtils.EMPTY);
    properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
    properties.setProperty("hibernate.format_sql", "false");
    properties.setProperty("hibernate.show_sql", String.valueOf(showGeneratedSql));
    properties.setProperty("hibernate.use_sql_comments", String.valueOf(showGeneratedSql));

    return properties;
  }
}
