package us.javaj;

import java.io.File;
import java.io.IOException;

import javax.servlet.MultipartConfigElement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import us.javaj.magnus.beans.FileUploadBean;


@Configuration
public class ConfigureViewLayer {
  private static final Log LOG = LogFactory.getLog(ConfigureViewLayer.class);


  @Bean
  @Autowired
  public MultipartConfigElement multipartConfigElement(FileUploadBean fileUpload) throws IOException {
    MultipartConfigFactory factory = new MultipartConfigFactory();
    File uploadDirectory = new File(fileUpload.getUploadLocation());
    factory.setLocation(uploadDirectory.getCanonicalPath());
    factory.setMaxFileSize(DataSize.ofMegabytes(fileUpload.getMaxFileSizeMb()));
    factory.setMaxRequestSize(DataSize.ofMegabytes(fileUpload.getMaxFileSizeMb() * 2));
    factory.setFileSizeThreshold(DataSize.ofMegabytes(fileUpload.getMaxFileSizeMb() / 2));
    MultipartConfigElement multipartConfig = factory.createMultipartConfig();
    logUploadDirectory(multipartConfig);

    return multipartConfig;
  }


  @Bean
  @Autowired
  public MultipartResolver multipartResolver(FileUploadBean fileUpload) {
    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
    multipartResolver.setMaxUploadSize(fileUpload.getMaxFileSizeBytes());
    return multipartResolver;
  }


  @Bean
  public FreeMarkerConfigurer freeMarkerConfigurer() {
    String[] paths = new String[] { "/macros", "/templates" };
    FreeMarkerConfigurer config = new FreeMarkerConfigurer();
    config.setPreferFileSystemAccess(false);
    config.setTemplateLoaderPaths(paths);
    config.setDefaultEncoding("UTF-8");
    return config;
  }


  @Bean
  public FreeMarkerViewResolver freeMarkerViewResolver() {
    FreeMarkerViewResolver resolver = new FreeMarkerViewResolver();
    resolver.setCache(true);
    resolver.setPrefix("");
    resolver.setSuffix(".fm");
    resolver.setContentType("html");
    resolver.setExposeSpringMacroHelpers(true);
    resolver.setExposeRequestAttributes(false);
    resolver.setExposeSessionAttributes(false);
    return resolver;
  }


  private void logUploadDirectory(MultipartConfigElement config) {
    String dash = "-";
    StringBuilder sb = new StringBuilder();
    sb.append("Configuring the following file upload parameters");
    sb.append("\n").append(dash.repeat(58));
    sb.append("\n  upload location: ").append(config.getLocation());
    sb.append("\n request max size: ").append(config.getMaxRequestSize()).append(" bytes");
    sb.append("\n    max file size: ").append(config.getMaxFileSize()).append(" bytes");
    sb.append("\n  write threshold: ").append(config.getFileSizeThreshold()).append(" bytes");
    sb.append("\n").append(dash.repeat(32));
    LOG.info(sb.toString());
  }
}
