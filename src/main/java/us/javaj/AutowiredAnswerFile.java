package us.javaj;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import us.javaj.magnus.sample.demo.JavaNineExample;
import us.javaj.magnus.sample.demo.UserFileReader;


@Configuration
public class AutowiredAnswerFile {

  @Bean
  public JavaNineExample javaNine(@Autowired @Qualifier("ultimateAnswer") UserFileReader userFile) {
    return new JavaNineExample(userFile);
  }
  
  @Bean("ultimateAnswer")
  public UserFileReader registerFile1() throws IOException {
    return new UserFileReader("Sandbox/answer.txt");
  }
}
