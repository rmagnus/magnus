package us.javaj;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.session.jdbc.JdbcIndexedSessionRepository;
import org.springframework.session.web.http.SessionRepositoryFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;


/**
 * Create two filter chains that cater to various session authentication and
 * session attribute needs. For project Clovis, the remake of King Floyd's
 * Empire, sessions are store in the database so they can be resumed when
 * a particular user's session crashes or their Internet simply goes out.
 */
@EnableWebSecurity
public class DualSessionStrategy {
  final static String SLASH_STRS  = "/**";
  final static String CLOVIS  = "/clovis";

  
  @Order(1)
  @Configuration
  public static class JavaJConfigurationAdapter extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

      http.antMatcher(SLASH_STRS)
          .csrf().disable()
          .logout().disable()
          .headers().disable()
          .anonymous().disable()
          .requestCache().disable()
          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
    }
  }


  @Order(2)
  @Configuration
  public static class ClovisSessionConfigurationAdapter extends WebSecurityConfigurerAdapter {
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {

      http.antMatcher(CLOVIS + SLASH_STRS)
          .csrf().disable()
          .logout().disable()
          .headers().disable()
          .anonymous().disable()
          .requestCache().disable()
          .exceptionHandling().disable()
          .sessionManagement().maximumSessions(1);
    }
  }


  @EnableScheduling
  @Configuration(proxyBeanMethods = false)
  public class SessionCleanupConfiguration implements SchedulingConfigurer {
    private final static String DEFAULT_CLEANUP_CRON = "0 * * * * *";

    private final JdbcIndexedSessionRepository sessionRepo;

    SessionCleanupConfiguration(JdbcIndexedSessionRepository sessionRepo) {
      this.sessionRepo = sessionRepo;
    }

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
      taskRegistrar.addCronTask(sessionRepo::cleanUpExpiredSessions,
          DEFAULT_CLEANUP_CRON);
    }
  }


  /**
   * Removes the forced database session for request that are not KFR oriented.
   * Without this filter, each session and attributes would be serialized to
   * the database, due to the nature of JDBC session configuration defaults.
   */
  @Component
  @Order(Integer.MIN_VALUE)
  public static class ExcludeSessionRepositoryFilter extends OncePerRequestFilter {
    private final String SRF_CLASS = SessionRepositoryFilter.class.getCanonicalName();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
        FilterChain filterChain) throws ServletException, IOException {
      if (!request.getRequestURI().startsWith(CLOVIS)) {
        request.setAttribute(SRF_CLASS.concat(".FILTERED"), Boolean.TRUE);
      }
      filterChain.doFilter(request, response);
    }
  }
}
