package us.javaj.data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;


@MappedSuperclass
public abstract class UniqueEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  protected Integer id;

  @Transient
  private volatile Object uid;

  public Integer getId() {
    return id;
  }


  /**
   * Gerry Power method (forum.hibernate.org) for determining uniqueness for
   * both pre-persisted classes along side persisted objects within the same
   * collection. A volatile object will force access to be synchronized.
   *
   * @return Object
   */
  private Object getObject() {
    if (uid != null || uid == null && id == null) {
      if (uid == null) {
        synchronized(this) {
          if (uid == null) {
            uid = new Object();
          }
        }
      }
      return uid;
    }
    return id;
  }


  @Override
  public boolean equals(Object obj) {
    final boolean toReturn;
    if (obj instanceof UniqueEntity) {
      toReturn = this.getObject().equals(((UniqueEntity)obj).getObject());
    }
    else {
      toReturn = false;
    }
    return toReturn;
  }


  @Override
  public int hashCode() {
    return this.getObject().hashCode();
  }
}
