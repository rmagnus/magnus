package us.javaj.data;

import org.apache.commons.lang3.StringUtils;

/**
 * Something that knows how to generate Web links and email addresses
 * and such as this.
 */
public interface WebSavvy {

  static final String HTTPS  = "https://";
  static final String HTTP   = "http://";

  default String linkify(String site) {
    if (StringUtils.startsWithAny(site, HTTP, HTTPS)) {
      return site;
    }
    return HTTP.concat(site);
  }
}
