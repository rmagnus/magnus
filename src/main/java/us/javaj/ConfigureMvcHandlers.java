package us.javaj;

import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import us.javaj.magnus.web.servlet.ModelAttributeHandler;
import us.javaj.magnus.web.servlet.NavigationHandler;


@Configuration
public class ConfigureMvcHandlers extends WebMvcConfigurationSupport {

  @Bean
  NavigationHandler buildNavigationHandler() {
    return new NavigationHandler();
  }

  @Bean
  ModelAttributeHandler buildModelAttributeHandler() {
    return new ModelAttributeHandler();
  }

  /*
   * Add this back in when ready to work with custom session attributes.
   *
  @Override
  protected RequestMappingHandlerAdapter createRequestMappingHandlerAdapter() {
    SessionAttributeHandler handlerAdapter = new SessionAttributeHandler();
    handlerAdapter.setSessionAttributeStore(new SessionStoreStrategy());

    return handlerAdapter;
  }
  */

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(buildNavigationHandler()).addPathPatterns("/**").excludePathPatterns("/_*/**");
    registry.addInterceptor(buildModelAttributeHandler()).addPathPatterns("/**").excludePathPatterns("/_*/**");
  }

  /**
   * Static resource mapping(s). This replaces the old XML configuration way:
   *
   * &lt;mvc:resources mapping="/_css/**" location="/_styles/"/&rt;
   * &lt;mvc:resources mapping="/_img/**" location="/_images/"/&rt;
   * &lt;mvc:resources mapping="/_js/**" location="/_scripts/"/&rt;
   *
   * JavaJ will have as many as three or four different layouts until the time
   * when they actually will come together under one common theme or template
   * (but who really knows)? These resource handlers will hopefully simplify
   * resource location starting prefixes when specifying them in a FreeMarker
   * template (in say the HTML head element or in each individual page).
   *
   * Prefix [/_e/css] is shorthand for [/src/main/webapp/static/enkel/css/].
   * Prefix [/_p/css] is shorthand for [/src/main/webapp/static/progressus/css].
   * Prefix [/_t/css] is shorthand for [/src/main/webapp/static/theme/css].
   * Same goes for fonts, img, and js following the same pattern above.
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/_css/**").addResourceLocations("/static/css/");
    registry.addResourceHandler("/_img/**").addResourceLocations("/static/img/");
    registry.addResourceHandler("/_js/**").addResourceLocations("/static/js/");

    List.of("theme", "enkel", "progressus").stream().forEach(t -> {
      registry.addResourceHandler("/_".concat(t.substring(0, 1)).concat("/css/**"))
          .addResourceLocations("/static/_".concat(t).concat("/css/"));
      registry.addResourceHandler("/_".concat(t.substring(0, 1)).concat("/fonts/**"))
          .addResourceLocations("/static/_".concat(t).concat("/fonts/"));
      registry.addResourceHandler("/_".concat(t.substring(0, 1)).concat("/img/**"))
          .addResourceLocations("/static/_".concat(t).concat("/img/"));
      registry.addResourceHandler("/_".concat(t.substring(0, 1)).concat("/js/**"))
          .addResourceLocations("/static/_".concat(t).concat("/js/"));
    });
  }
}
