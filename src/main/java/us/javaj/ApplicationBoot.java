package us.javaj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import us.javaj.magnus.beans.CommonProps;


@SpringBootApplication(exclude = { UserDetailsServiceAutoConfiguration.class })
public class ApplicationBoot extends SpringBootServletInitializer {

  @Bean("props")
  public CommonProps provideCommonProperties() {
    return new CommonProps();
  }

  public static void main(String[] args) {
    new SpringApplication(ApplicationBoot.class).run(args);
  }
}
