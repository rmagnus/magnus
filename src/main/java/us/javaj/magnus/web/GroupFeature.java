package us.javaj.magnus.web;

public interface GroupFeature {

  String getTitle();

  String getHtml();

  String getImage();

  String getDate();

}
