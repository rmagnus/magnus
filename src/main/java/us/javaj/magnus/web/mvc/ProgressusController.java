package us.javaj.magnus.web.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/progressus")
public class ProgressusController {

  @GetMapping
  public String displayProgressus(Model model) {
    return "bootstrap/progressus";
  }

  @GetMapping("/samp")
  public String displayProgressusTwo(Model model) {
    return "bootstrap/progressus-samp";
  }
}
