package us.javaj.magnus.web.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class IndexController {

  @GetMapping("/")
  public String forwardToStreams() {
    return "forward:/homepage";
  }

  @ResponseBody
  @GetMapping("favicon.ico")
  void returnNoFavicon() {
    return;
  }
}
