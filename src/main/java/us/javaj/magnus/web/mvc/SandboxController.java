package us.javaj.magnus.web.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import us.javaj.magnus.service.ApplicationService;
import us.javaj.magnus.service.CreateProfileService;
import us.javaj.vinyl.service.VinylCollectionService;


@Controller
@RequestMapping("/sandbox")
public class SandboxController {

  @Autowired
  ApplicationService appService;

  @Autowired
  CreateProfileService profileService;

  @Autowired
  VinylCollectionService vinylService;


  /**
   * Working on display a feature component. Want to be able to edit
   * the markdown and save and then instantly see the HTML rendering.
   *
   * @param model
   * @return view
   */
  @GetMapping
  public String displayProjectsPage(Model model) {
    return "sandbox/projects";
  }

  /**
   * A simple test integrating the Vinyl project into the brand new template.
   *
   * @param model
   * @return view
   */
  @GetMapping("/features")
  public String displayFeaturesTestPage(Model model) {
    model.addAttribute("profile", profileService.fetchProfileModel(2));
    //model.addAttribute("message", appService.fetchFeatureMessage(1));
    model.addAttribute("quote", appService.fetchRandomAuthorQuoteFeatureModel("tolkien"));
    model.addAttribute("article", appService.fetchFeatureArticle(1));
    model.addAttribute("feature", appService.fetchFeatureModel("walkies"));

    return "sandbox/features";
  }

  /**  
   * A simple test integrating the Vinyl project into the brand new template.
   *
   * @param model
   * @return view
   */
  @GetMapping("/booknook")
  public String displayVinylRecordsThinger(Model model) {
    model.addAttribute("records", vinylService.getVinylRecords());
    return "sandbox/booknook";
  }

  /**
   * Medical information regarding my condition in 2020
   *
   * @param model
   * @return
   */
  @GetMapping("/medical")
  public String displayMedicalInfo(Model model) {
    return "sandbox/medical";
  }
}
