package us.javaj.magnus.web.mvc;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import us.javaj.magnus.service.FileStorageService;
import us.javaj.magnus.web.profile.ProfileFormController;


@Controller
public class ImageController {
  private static final Logger LOG = LoggerFactory.getLogger(ProfileFormController.class);

  @Autowired
  FileStorageService fileStorage;


  @GetMapping("/download/{fileName:.+}")
  public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {

    // Load file as Resource
    Resource resource = fileStorage.loadFileAsResource(fileName);
    String contentDisp = "attachment; filename=\"?\"".replace("?", resource.getFilename());

    // Try to determine file's content type
    String contentType = null;
    try {
      contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
    }
    catch (IOException ex) {
      LOG.info("Could not determine file type.");
    }

    // Fallback to the default content type
    if (contentType == null) {
      contentType = "application/octet-stream";
    }

    LOG.info("Download content type {}", contentType);
    LOG.info("Download content disposition {}", contentDisp);

    return ResponseEntity.ok()
      .contentType(MediaType.parseMediaType(contentType))
      .header(HttpHeaders.CONTENT_DISPOSITION, contentDisp)
      .body(resource);
  }
}
