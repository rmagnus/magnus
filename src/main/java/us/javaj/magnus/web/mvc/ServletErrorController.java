package us.javaj.magnus.web.mvc;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import us.javaj.clovis.GameException;


//@Controller
public class ServletErrorController { // implements ErrorController {
  private static final Logger LOG = LoggerFactory.getLogger(ServletErrorController.class);

  /**
   * This handler is for application server errors and Spring dispatcher
   * exception. Note that this handler is mapped to the same page as the
   * GameException exception handler found in GameController.
   *
   * @param request
   * @param model
   * @return view
   */
  @RequestMapping("/error")
  public String handleServletExceptions(HttpServletRequest request, Model model) {
    Integer statusCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
    Exception exception = (Exception)request.getAttribute("javax.servlet.error.exception");
    if (statusCode != null && exception == null) {
      HttpStatus status = HttpStatus.resolve(statusCode);
      exception = new GameException(status.getReasonPhrase());
    }
    return checkedErrorPath(request.getRequestURL().toString(), statusCode, exception, model);
  }


  private String checkedErrorPath(String url, Integer status, Exception error, Model model) {
    LOG.warn("Error with request {} [{}:{}]", url, status, error);
    model.addAttribute("status", status);
    model.addAttribute("error", error);

    return "/clovis/error";
  }


  /*
  @Override
  public String getErrorPath() {
    return "/clovis/error";
  }
  */
}
