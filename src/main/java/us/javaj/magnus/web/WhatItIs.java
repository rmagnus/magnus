package us.javaj.magnus.web;

import us.javaj.magnus.data.domain.Feature;
import us.javaj.magnus.data.domain.Profile;


public abstract class WhatItIs {

  /* Must set this property at contruction time or bad things happen */
  protected ModelType type;
  
  /* Model scope -- rare site-wide global or plain old group based */
  private boolean sitewide;


  /**
   * Contruction helper for 'type' field.
   *
   * @param feature
   */
  protected WhatItIs(Feature feature) {
    type = ModelType.FEATURE;
    // reanalyze this logic
    if (feature.getArticle() == null) {
      sitewide = true;
    }

    /*
    if (feature.getMessage() != null) {
      type = ModelType.REPLY;
    }
    */
    if (feature.getArticle() != null) {
      type = ModelType.ARTICLE;
    }
  }

  protected WhatItIs(Profile profile) {
    type = ModelType.PROFILE;
  }


  public boolean isSitewide() {
    return sitewide;
  }

  public boolean isThis(ModelType type) {
    return this.type == type;
  }

  public boolean isFeature() {
    return isThis(ModelType.FEATURE);
  }

  public boolean isMessage() {
    return isThis(ModelType.MESSAGE);
  }

  public boolean isArticle() {
    return isThis(ModelType.ARTICLE);
  }

  public boolean isProfile() {
    return isThis(ModelType.PROFILE);
  }
}
