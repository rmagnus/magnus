package us.javaj.magnus.web;


/**
 * The main JavaJ web application will need some navigation 'spook' to know
 * where the current user is and where they can go next. I would think one
 * level deep is enough to which to begin, but sandbox elements may travel
 * deeper IDK. 
 *
 * @author <a href="justin@javaj.us">javaj</a>
 */
public class NavigationSpook {
  
}
