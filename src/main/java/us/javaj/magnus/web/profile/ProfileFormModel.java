package us.javaj.magnus.web.profile;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import us.javaj.magnus.data.domain.About;


/**
 * Object for which to bind profile data as user works through
 * the create profile page. Images are uploaded temporarily to
 * the server with references to the files here. Eventually 
 */
public class ProfileFormModel {
  private static final Logger LOG = LoggerFactory.getLogger(ProfileFormController.class);

  @Size(min = 5, max = 40, message = "Your profile name must between 5 and 40 characters")
  @NotEmpty(message = "Please enter a profile name for display")
  private String name;

  @Size(min = 40, max = 140, message = "Your phrase or idiom name must between 40 and 140 characters")
  @NotEmpty(message = "Please enter a clever phrase or descriptive idiom")
  private String idiom;

  @NotEmpty(message = "Please upload an image for your profile picture or avatar")
  private String avatarFile;
  private String avatarEndpoint;

  @NotEmpty(message = "Please upload a landscape-sized background image")
  private String backgroundFile;
  private String backgroundEndpoint;

  private List<About> abouts;

  ProfileFormModel() {
    abouts = new ArrayList<>(4);
    abouts.add(new About("Went to"));
    abouts.add(new About("Works at"));
    abouts.add(new About("Lives in"));
    abouts.add(new About("From"));
  }


  // Accessors
  public String getName() {
    return name;
  }

  public String getName(String defaultValue) {
    return StringUtils.defaultIfBlank(name, defaultValue);
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIdiom() {
    return idiom;
  }

  public String getIdiom(String defaultValue) {
    return StringUtils.defaultIfBlank(idiom, defaultValue);
  }

  public void setIdiom(String idiom) {
    this.idiom = idiom;
  }

  public String getAvatarFile() {
    return avatarFile;
  }

  public String getAvatarEndpoint() {
    return avatarEndpoint;
  }

  public String getAvatarEndpoint(String defaultValue) {
    return StringUtils.defaultIfBlank(avatarEndpoint, defaultValue);
  }

  public String getBackgroundFile() {
    return backgroundFile;
  }

  public String getBackgroundEndpoint() {
    return backgroundEndpoint;
  }

  public String getBackgroundEndpoint(String defaultValue) {
    return StringUtils.defaultIfBlank(backgroundEndpoint, defaultValue);
  }

  void setImageValues(String type, String endpoint) {
    if ("avatar".equals(type)) {
      avatarFile = StringUtils.right(endpoint, endpoint.lastIndexOf("/"));
      avatarEndpoint = endpoint;
    }
    else if ("background".equals(type)) {
      backgroundFile = StringUtils.right(endpoint, endpoint.lastIndexOf("/"));
      backgroundEndpoint = endpoint;
    }
    LOG.debug("[avatar {}], [background {}]", avatarEndpoint, backgroundEndpoint);
  }

  void resetImageValues(String type) {
    if ("avatar".equals(type)) {
      avatarFile = null;
      avatarEndpoint = null;
    }
    else if ("background".equals(type)) {
      backgroundFile = null;
      backgroundEndpoint = null;
    }
  }

  public List<About> getAbouts() {
    return abouts;
  }

  public void setAbouts(List<About> abouts) {
    this.abouts = abouts;
  }
}
