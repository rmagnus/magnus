package us.javaj.magnus.web.profile;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import us.javaj.magnus.service.CreateProfileService;
import us.javaj.magnus.web.ModelHelper;


/**
 * The profile controller is what handles displaying the current profile a user
 * is signed under -or- the one requested as a request parameter token. A number
 * of profile actions a signed-in user can take are: 1) Change profile info such
 * as name and idiom messages, 2) Upload new images for sharing in media streams
 * and 3) other things not even thought of yet.
 *
 * @author <a href="justin@javaj.us">javaj</a>
 */
@Controller
@SessionAttributes("profile")
public class ProfileController {

  @Autowired
  CreateProfileService profileService;


  @GetMapping("/profile")
  public String displayHomepageStream() {
    return "bootstrap/profile";
  }


  @GetMapping("/profile/welcome")
  public String displayWelcomePage(Model model, HttpSession session) {
    if (model.containsAttribute("profileId")) {
      Integer profileId = (Integer) model.getAttribute("profileId");
      ProfileModel profile = profileService.fetchProfileModel(profileId);
      simulateOnAuthenticationSuccess(session, profile);
    }
    // strictly temporary
    else {
      ProfileModel profile = profileService.fetchProfileModel(5);
      simulateOnAuthenticationSuccess(session, profile);
    }

    ModelHelper.addToModel("profile", model, session);
    return "bootstrap/profile-welcome";
  }


  /**
   * Later this should be handled by Spring Security onAuthenticationSuccess
   * once an user has been authenticated and their profile info retrieved.
   *
   * @param session
   * @param profile
   */
  private void simulateOnAuthenticationSuccess(HttpSession session,
      ProfileModel profile) {
    session.setAttribute("profile", profile);
  }
}
