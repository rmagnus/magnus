package us.javaj.magnus.web.profile;

import java.io.IOException;
import java.io.InputStream;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import us.javaj.magnus.data.domain.Profile;
import us.javaj.magnus.service.CreateProfileService;
import us.javaj.magnus.service.FileStorageService;


@Controller
@SessionAttributes("profile")
public class ProfileFormController {
  private static final Logger LOG = LoggerFactory.getLogger(ProfileFormController.class);

  @Autowired
  CreateProfileService profileService;
  @Autowired
  FileStorageService fileStorage;


  @ModelAttribute("profile")
  public ProfileFormModel profileModelForBinding() {
    LOG.info("in profileModelForBinding()...");

    return new ProfileFormModel();
  }


  @GetMapping("/profile/create")
  public String displayProfileForm(ProfileFormModel profile) {
    LOG.info("in displayProfileForm()...");

    return "bootstrap/profile-create";
  }


  @PostMapping("/profile/create")
  public String bindAndSaveProfile(@Valid @ModelAttribute("profile") ProfileFormModel profileModel,
    BindingResult result, RedirectAttributes attributes) {
    LOG.info("in bindAndSaveProfile()...");
    if (result.hasErrors()) {
      return "bootstrap/profile-create";
    }

    Profile profile = profileService.createNewProfile(profileModel);
    attributes.addFlashAttribute("profileId", profile.getId());

    return "redirect:/profile/welcome";
  }


  @PostMapping("/profile/upload")
  public ResponseEntity<String> uploadMultipartFile(@ModelAttribute("profile") ProfileFormModel profile,
      @RequestParam("image") MultipartFile image, @RequestParam("type") String type) throws Exception {
    String retrievalEndpoint = fileStorage.storeMultipartFile(image);
    profile.setImageValues(type, retrievalEndpoint);
    logImageInfo(image, retrievalEndpoint);

    return new ResponseEntity<String>(retrievalEndpoint, HttpStatus.OK);
  }


  @PostMapping("/profile/reset")
  public ResponseEntity<String> resetMultipartFile(@ModelAttribute("profile") ProfileFormModel profile,
      @RequestParam("type") String type) {
    profile.resetImageValues(type);

    return new ResponseEntity<String>(type, HttpStatus.OK);
  }


  private void logImageInfo(MultipartFile image, String imageEndpoint) throws IOException {
    InputStream inputStream = image.getInputStream();
    String originalName = image.getOriginalFilename();
    String name = image.getName();
    String contentType = image.getContentType();
    long size = image.getSize();

    LOG.info(" inputStream: " + inputStream);
    LOG.info("originalName: " + originalName);
    LOG.info("        name: " + name);
    LOG.info(" contentType: " + contentType);
    LOG.info("        size: " + size);
    LOG.info("    endpoint: " + imageEndpoint);
  }
}
