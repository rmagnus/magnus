package us.javaj.magnus.web.profile;

import org.apache.commons.lang3.StringUtils;

import us.javaj.magnus.data.domain.Profile;
import us.javaj.magnus.web.GroupFeature;
import us.javaj.magnus.web.WhatItIs;


public class ProfileModel extends WhatItIs implements GroupFeature {
  public static final String DEFAULT_NAME = 
      "Enter an profile name for display here";
  public static final String DEFAULT_IDIOM = 
      "Enter an idiom or phrase that describes you for display here";

  /*
   * First name or display name of a combination of first and last 
   */
  private String name;

  /*
   * Something to live by or swear by or just be silly
   */
  private String idiom;
  
  /*
   * Profile card image (almost always profile avatar image)
   */
  private String cardImage;

  /*
   * Profile header image which can be null (white)
   */
  private String headerImage;

  /*
   * Month and year the profile was first established
   */
  private String memberSince;

  public ProfileModel(Profile profile) {
    super(profile);
    resolveName(profile);
    resolveIdiom(profile);

    cardImage = profile.getAvatarUrl();
    headerImage = profile.getBackdropUrl();
  }


  /**
   * Contruction helper for 'name' field.
   *
   * @param profile
   */
  private void resolveName(Profile profile) {
    name = DEFAULT_NAME;

    if (StringUtils.isNotBlank(profile.getDisplayName())) {
      name = profile.getDisplayName().trim();
    }
    else if (StringUtils.isNotBlank(profile.getLastName())) {
      name.concat(" ").concat(profile.getLastName().trim());
    }
  }

  /**
   * Contruction helper for 'idiom' field.
   *
   * @param feature
   */
  private void resolveIdiom(Profile profile) {
    idiom = DEFAULT_IDIOM;

    if (StringUtils.isNotBlank(profile.getIdiom())) {
      idiom = profile.getIdiom();
    }
  }


  @Override
  public String getTitle() {
    return name;
  }

  @Override
  public String getHtml() {
    return idiom;
  }

  public String getHeaderImage() {
    return headerImage;
  }

  @Override
  public String getImage() {
    return cardImage;
  }

  @Override
  public String getDate() {
    return memberSince;
  }
}
