package us.javaj.magnus.web;

public enum ModelType {

  /*
   * Homepage features seen all all up and down the prominent middle column
   */
  SITE_FEATURE, GROUP_FEATURE, FEATURE, MESSAGE, REPLY, ARTICLE, COMMENT,

  /*
   * Smaller, recurring elements shown as square-like cards in familiar positions
   */
  PROFILE, PROJECT, FIRESIDE, QUOTE, VINYL_RECORD, BOOK_NOOK,

  /*
   * Not sure these will be useful but provided to provide context and scope
   */
  ABOUT, COPYRIGHT, SPONSORED, UPCOMING, GROWL;

}
