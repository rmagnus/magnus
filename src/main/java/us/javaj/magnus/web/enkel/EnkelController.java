package us.javaj.magnus.web.enkel;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * Enkel was the JavaJ theme prior to the lastest Bootstrap Application theme.
 * Some pages will continue to use this theme as a springboard to unfinished
 * development.
 *
 * @author <a href="justin@javaj.us">javaj</a>
 */
@Controller
public class EnkelController {

  @GetMapping("/enkel")
  public String handleRequest(Model model) {
    model.addAttribute("enkel", new EnkelModel());

    return "bootstrap/enkel";
  }
}
