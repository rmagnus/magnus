package us.javaj.magnus.web.enkel;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;


/**
 * Static model that knows all about the Enkel Bootstrap theme, for the halibut.
 */
public class EnkelModel {
  private static final CascadingStyles[] styles;
  private static final GoogleFont[] fonts;

  static {
    styles = new CascadingStyles[] {
        new CascadingStyles("bootstrap.min.css"),
        new CascadingStyles("font-awesome.css"),
        new CascadingStyles("plugins/animate.css"),
        new CascadingStyles("plugins/flexslider.css"),
        // ------------------------------------- //
        new CascadingStyles("theme-style.css"),
        new CascadingStyles("custom-style.css"),
        new CascadingStyles("colour-green.css")
    };

    fonts = new GoogleFont[] {
        new GoogleFont("Source+Code+Pro", 300),
        new GoogleFont("Roboto", 300, 700),
        new GoogleFont("Monda", 400, 700)
    };
  }

  public String fontsMarkup() {
    return GoogleFont.toString(fonts);
  }
  
  public String cssMarkup() {
    return CascadingStyles.toString(styles);
  }
  
  public static void main(String... args) {
    EnkelModel e = new EnkelModel();
    System.out.println(e.fontsMarkup());
    System.out.println(e.cssMarkup());
  }

  static class GoogleFont {
    private String name;
    private int[] sizes;

    GoogleFont(String name, int... sizes) {
      this.name = name;
      this.sizes = sizes;
    }

    static String toString(GoogleFont[] fonts) {
      StringBuffer sb = new StringBuffer("<!-- Google Fonts -->");
      Arrays.asList(fonts).forEach(sb::append);
      return sb.toString();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder(StringUtils.LF);
      sb.append("<link href=\"http://fonts.googleapis.com/css?family=");
      sb.append(name).append(":").append(StringUtils.join(sizes, ','));
      sb.append("\" rel=\"stylesheet\" type=\"text/css\">");
      return sb.toString();
    }
  }
  
  static class CascadingStyles {
    private String name;

    CascadingStyles(String name) {
      this.name = name;
    }

    static String toString(CascadingStyles[] styles) {
      StringBuffer sb = new StringBuffer();
      sb.append("<!-- Bootstrap, Font Awesome, Theme -->");
      Arrays.asList(styles).forEach(sb::append);
      return sb.toString();
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder(StringUtils.LF);
      sb.append("<link href=\"/_e/css/").append(name);
      sb.append("\" rel=\"stylesheet\">");
      return sb.toString();
    }
  }
}
