package us.javaj.magnus.web;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;


public class ModelHelper {

  public static void addToModel(String attributeName, Model model, HttpSession session) {
    if (session.getAttribute(attributeName) == null) {
      throw new IllegalArgumentException("No attribute found for name " + attributeName);
    }
    model.addAttribute(attributeName, session.getAttribute(attributeName));
  }
}
