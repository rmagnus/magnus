package us.javaj.magnus.web.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.web.bind.support.SessionAttributeStore;
import org.springframework.web.context.request.WebRequest;


public class SessionStoreStrategy implements SessionAttributeStore {
  private static final Logger LOG = LoggerFactory.getLogger(SessionAttributeStore.class);

  private String attributeNamePrefix = "sms_";

  private String getAttributeNameInSession(WebRequest request, String attributeName) {
    return this.attributeNamePrefix + attributeName;
  }


  @Override
  public void storeAttribute(WebRequest request, String name, Object value) {
    Assert.notNull(request, "WebRequest must not be null");
    Assert.notNull(name, "Attribute name must not be null");
    Assert.notNull(value, "Attribute value must not be null");

    String storageName = getAttributeNameInSession(request, name);
    LOG.debug("Storing {} under attribute {}", value.getClass().getCanonicalName(), storageName);
    request.setAttribute(storageName, value, WebRequest.SCOPE_SESSION);
  }


  @Override
  @Nullable
  public Object retrieveAttribute(WebRequest request, String name) {
    Assert.notNull(request, "WebRequest must not be null");
    Assert.notNull(name, "Attribute name must not be null");

    String storageName = getAttributeNameInSession(request, name);
    Object fromSession = request.getAttribute(storageName, WebRequest.SCOPE_SESSION);
    if (fromSession != null) {
      LOG.debug("Retrieving {} under attribute {}", fromSession.getClass().getCanonicalName(), storageName);
    }
    else {
      LOG.debug("Nothing found under attribute {}");
    }
    return fromSession;
  }


  @Override
  public void cleanupAttribute(WebRequest request, String name) {
    LOG.debug("Cleaning attribute {} from session storage");

    Assert.notNull(request, "WebRequest must not be null");
    Assert.notNull(name, "Attribute name must not be null");
    String storageName = getAttributeNameInSession(request, name);
    request.removeAttribute(storageName, WebRequest.SCOPE_SESSION);
  }
}
