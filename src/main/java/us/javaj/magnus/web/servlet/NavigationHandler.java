package us.javaj.magnus.web.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * Create session-based navigation 'spook' that travels with the user, keeping
 * track of where they may be at any particular moment. Helps the skin to know
 * what to context highlight as well (the main purpose for this class).
 */
public class NavigationHandler extends HandlerInterceptorAdapter {
  static final Logger LOG = LoggerFactory.getLogger(NavigationHandler.class);


  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler) throws Exception {
    return true;
  }
}
