package us.javaj.magnus.web.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import us.javaj.magnus.beans.CommonProps;


public class ModelAttributeHandler extends HandlerInterceptorAdapter {
  static final Logger LOG = LoggerFactory.getLogger(ModelAttributeHandler.class);

  @Autowired
  private CommonProps commonProps;


  /**
   * Place common application attributes on the model. These include things like
   * HTML title elements and meta data. These application attributes will grow
   * or shrink over time based upon their general usefullness.
   *
   * @param request
   * @param response
   * @param handler
   * @param modelAndView
   */
  @Override
  public void postHandle(HttpServletRequest request, HttpServletResponse response,
      Object handler, ModelAndView modelAndView) throws Exception {
    if (modelAndView != null) {
      modelAndView.addObject("props", commonProps);
    }
  }
}
