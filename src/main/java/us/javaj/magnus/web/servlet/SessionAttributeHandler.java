package us.javaj.magnus.web.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;


public class SessionAttributeHandler extends RequestMappingHandlerAdapter {
  private static final Logger LOG = LoggerFactory.getLogger(SessionAttributeHandler.class);

  
  @Override
  protected boolean supportsInternal(HandlerMethod handlerMethod) {
    LOG.info("\n============\n supportsInternal --> " + handlerMethod);

    return true;
  }


  @Override
  protected ModelAndView handleInternal(HttpServletRequest request, HttpServletResponse response,
      HandlerMethod handlerMethod) throws Exception {
    ModelAndView modelAndView = super.handleInternal(request, response, handlerMethod);

    return modelAndView;
  }
}
