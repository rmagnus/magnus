package us.javaj.magnus.web;

public interface RecurringFeature {

  String getTitle();

  String getImage();

  String getHtml();

  
}
