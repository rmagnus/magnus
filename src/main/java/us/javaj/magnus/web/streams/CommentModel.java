package us.javaj.magnus.web.streams;

import java.text.SimpleDateFormat;

import io.micrometer.core.instrument.util.StringUtils;
import us.javaj.magnus.data.MarkdownCapable;
import us.javaj.magnus.data.domain.Message;
import us.javaj.magnus.web.FeatureComment;


/**
 * Can be used as a standalone message or feature slash article comment.
 */
public class CommentModel implements FeatureComment, MarkdownCapable {
  public static final String DEFAULT_TITLE     = "Some Comment";
  public static final String DEFAULT_MARKDOWN  = "Needs Markdown";

  /*
   * Message title which to start will be profile name
   */
  private String title;

  /*
   * Message image (probably always profile avatar image)
   */
  private String avatar;

  /*
   * Markdown that will be rendered by a Marked-J library
   */
  private String markdown;

  /*
   * Shorthand date of when the message was posted
   */
  private String date;

  CommentModel() { }

  public CommentModel(Message message) {
    resolveTitle(message);
    resolveImage(message);
    resolveMarkdown(message);
    resolveDisplayDate(message);
  }

  private void resolveTitle(Message message) {
    title = DEFAULT_TITLE;

    if (message.getProfile() != null) {
      title = message.getProfile().getFirstName().trim();

      if (StringUtils.isNotBlank(message.getProfile().getDisplayName())) {
        title = message.getProfile().getDisplayName().trim();
      }
      else if (StringUtils.isNotBlank(message.getProfile().getLastName())) {
        title.concat(" ").concat(message.getProfile().getLastName().trim());
      }
    }
  }

  private void resolveImage(Message message) {
    avatar = message.getProfile().getAvatarUrl();
  }

  private void resolveMarkdown(Message message) {
    markdown = message.getMarkdown();
  }

  private void resolveDisplayDate(Message message) {
    date = new SimpleDateFormat("MMM d").format(message.getDateCreated());
  }


  // Accessors
  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String getHtml() {
    return toHtml(markdown);
  }

  @Override
  public String getAvatar() {
    return avatar;
  }

  @Override
  public String getDate() {
    return date;
  }

  @Override
  public boolean isComment() {
    return true;
  }
}
