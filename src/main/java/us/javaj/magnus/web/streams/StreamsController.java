package us.javaj.magnus.web.streams;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * Main controller that directs user request traffic through the homepage flow
 * (or stream) of JavaJ user content. The two chief flows here are 1) the main
 * homepage and 2) the subscription page where a user subscribes to one or more
 * user 'flows'.
 *
 * @author <a href="justin@javaj.us">javaj</a>
 */
@Controller
public class StreamsController {

  @GetMapping("/homepage")
  public String displayHomepageStream() {
    return "bootstrap/homepage";
  }


  @GetMapping("/streams")
  public String displayStreamSubscriptions() {
    return "bootstrap/streams";
  }
}
