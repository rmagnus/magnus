package us.javaj.magnus.web.streams;

import java.text.SimpleDateFormat;
import java.util.List;

import io.micrometer.core.instrument.util.StringUtils;
import us.javaj.magnus.data.MarkdownCapable;
import us.javaj.magnus.data.domain.Feature;
import us.javaj.magnus.web.FeatureComment;
import us.javaj.magnus.web.GroupFeature;
import us.javaj.magnus.web.WhatItIs;


/**
 * Features are shown in the main column on the homepage of the site. They can
 * be large messages, photo displays, links to showcased articles and the like.
 *
 * <p>Pass a Feature entity to construct. All the necessary fields that make
 * up the component model are set automagically, ready to render for the page!
 */
public class StreamItemModel extends WhatItIs implements GroupFeature, MarkdownCapable {
  public static final String DEFAULT_TITLE     = "Feature Title";

  /* Stream item title -- or when null, a profile name */
  private String title;

  /* Stream item image -- almost always Profile avatar image */
  private String image;

  /* Markdown that will be rendered using the Marked-J library */
  private String markdown;

  /* Stream item link -- if there is one */
  private String link;

  /* Shorthand date for when the item was created and posted */
  private String date;

  /* Any comments that have been posted after item was posted */
  private List<FeatureComment> comments;

  public StreamItemModel(Feature feature) {
    super(feature);
    resolveTitle(feature);
    resolveImage(feature);
    resolveLink(feature);
    resolveDisplayDate(feature);

    markdown = feature.getMarkdown();
    //markdown = isMessage() ? feature.getMessage().getMarkdown() : feature.getMarkdown();
  }


  /**
   * Contruction helper for 'title' field.
   *
   * @param feature
   */
  private void resolveTitle(Feature feature) {
    title = DEFAULT_TITLE;

    if (StringUtils.isNotBlank(feature.getTitle())) {
      title = feature.getTitle();
    }
    /*
    else if (isMessage()) {
      title = feature.getMessage().getProfile().getDisplayName();
    }
    */
    else if (isArticle()) {
      title = feature.getArticle().getTitle();
    }
  }

  /**
   * Contruction helper for 'image' location field.
   *
   * @param feature
   */
  private void resolveImage(Feature feature) {
    /*
    if (isMessage()) {
      image = feature.getMessage().getProfile().getAvatarUrl();
    }
    */
    if (StringUtils.isNotBlank(feature.getImageUrl())) {
      image = feature.getImageUrl();
    }
  }

  /**
   * Contruction helper for 'link' field.
   *
   * @param feature
   */
  private void resolveLink(Feature feature) {
    if (isArticle()) {
      link = feature.getArticle().getUri();
    }
  }

  /**
   * Contruction helper for 'date' field.
   *
   * @param feature
   */
  private void resolveDisplayDate(Feature feature) {
    SimpleDateFormat display = new SimpleDateFormat("MMM d");
    date = display.format(feature.getDateCreated());
    /*
    if (isMessage()) {
      date = display.format(feature.getMessage().getDateCreated());
    }
    else {
      date = display.format(feature.getDateCreated());
    }
    */
  }


  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String getHtml() {
    return toHtml(markdown);
  }

  @Override
  public String getImage() {
    return image;
  }

  @Override
  public String getDate() {
    return date;
  }

  public String getLink() {
    return link;
  }

  public List<FeatureComment> getComments() {
    return comments;
  }
}
