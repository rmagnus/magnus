package us.javaj.magnus.web;

public interface FeatureComment {

  String getTitle();

  String getHtml();

  String getAvatar();

  String getDate();

  boolean isComment();
}
