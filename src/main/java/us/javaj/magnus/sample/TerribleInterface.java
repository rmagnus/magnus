package us.javaj.magnus.sample;

import org.springframework.ui.Model;


/**
 * Here is a quick and dirty example of some Java 8 interface "default" methods. See this
 * @see <a href="https://www.journaldev.com/2752/java-8-interface-changes-static-method-default-method">link</a>
 * for a proper example. 
 * 
 * <p>Note: Returning HTML or any other form of markup from an interface and/or any other Java class in general
 * is likely very much frowned upon. Please never do this in real life...  
 */
public interface TerribleInterface {
  static final String CANNED_GREETING   = "Welcome to Spring Boot 2.0!";
  static final String RESTFUL_GREETING  = "Truly, this is a much improved greeting!";
  static final String DYNAMIC_GREETING  = "A More Exciting, Dynamic kind of Greeting";


  /**
   * Subclasses must implement an index-style request mapping for controller.
   * Response can take the form of a template or some rendered body 'output'.
   * 
   * @param model
   * @return response
   */
  String requestHandler(Model model);


  default String appendSomeUglyMarkup() {
    return "<pre><small style=\"color:gray\">&nbsp;&hellip; " +
           "as powered by " + this.getClass().getSimpleName() +
           "</small></pre>";
  }


  default String appendAnUglyHyperlink(String uri) {
    return "<pre><small style=\"color:gray\">&nbsp;&hellip; demo " +
           "to be continued by clicking <a href=\"" + uri + "\">"  +
           this.getClass().getSimpleName() + "</a></small></pre>";
  }
}
