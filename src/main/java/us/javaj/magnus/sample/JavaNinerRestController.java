package us.javaj.magnus.sample;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.javaj.magnus.sample.demo.ConsolePrinter;
import us.javaj.magnus.sample.demo.JavaNineExample;


@RestController
public class JavaNinerRestController {

  @Autowired
  private JavaNineExample javaNine;

  @Autowired
  private ConsolePrinter console;


  @GetMapping("/java/niner")
  public String startDemo(@RequestParam(value = "message", required=false) final String message) {

    // start the demo
    javaNine.demo();

    // print the message
    if (StringUtils.isNotEmpty(message)) {
      console.print(message);
    }

    return "Did you check the console for output?!";
  }
}
