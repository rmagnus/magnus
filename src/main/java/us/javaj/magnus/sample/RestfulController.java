package us.javaj.magnus.sample;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
public class RestfulController implements TerribleInterface {


  @Override
  @RequestMapping(value="/bad-example", method=RequestMethod.GET)
  public String requestHandler(Model model) {
    return "Hey, this isn't very RESTful...<p>We can do better!" + appendAnUglyHyperlink("/better-example");
  }


  @RequestMapping(value="/better-example", method=RequestMethod.GET)
  public ModelAndView requestHandler() {
    return new ModelAndView("presentation/restful");
  }


  /**
   * The star of our RestController is a JSON-mapped Greeting object.
   * 
   * @param model
   * @return Greeting
   */
  @GetMapping("/restful-greeting")
  public SimpleGreeting jsonGreeting(Model model) {
    return new SimpleGreeting(0, RESTFUL_GREETING);
  }
}
