package us.javaj.magnus.sample;


public class SimpleGreeting {

  private final long id;
  private final String content;

  public SimpleGreeting(long id, String content) {
    this.id = id;
    this.content = content;
  }


  // Accessors
  public long getId() {
    return id;
  }

  public String getContent() {
    return content;
  }
}