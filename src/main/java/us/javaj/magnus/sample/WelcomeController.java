package us.javaj.magnus.sample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class WelcomeController implements TerribleInterface {

  /**
   * Notice how this handler returns the name of the template to render.
   * 
   * @param model
   * @return view
   */
  @Override
  @GetMapping("/welcome")
  public String requestHandler(Model model) {
    model.addAttribute("greeting", CANNED_GREETING);

    return "presentation/sample";
  }


  /**
   * Notice how this handler returns void as the template is matched with the mapping.
   * 
   * @param model
   * @return view
   */
  @GetMapping("/greetings")
  public void dynamicHandler(@RequestParam(value="text", defaultValue=DYNAMIC_GREETING) String text, Model model) {
    model.addAttribute("greeting", text);

    // Code snippet and takeaways
    model.addAttribute("snippet", "/snippets/dynamic.fm");
    model.addAttribute("points", "/takeaways/params.fm");
  }
}
