package us.javaj.magnus.sample.demo;

import java.util.List;

import static java.util.function.Predicate.not;


public class JavaElevenExample {

  private static List<Integer> numbers = List.of(1, 2, 3, 5, 7, 9, 11); // since Java 9!

  private static ConsolePrinter console = new ConsolePrinter();

  /**
   * Simple example of the Java 11  Predicate.not() which returns
   * a predicate that is the negation of the supplied predicate. Neato!
   */
  public static void main(String[] args) {

    // try to add a number to the List seen above
    addNumberTo(numbers, 13);

    // display the type of List and 
    whatTypeIsThis(numbers);

    numbers.stream().filter(not(n -> n % 2 == 0)).forEach(console::print);
  }


  private static void addNumberTo(List<Integer> numbers, int number) {
    try { // to add a number to the list and fail ;)
      numbers.add(13);
    }
    catch (UnsupportedOperationException e) {
      console.print("Could not add unlucky number 13...");
    }
  }


  private static void whatTypeIsThis(List<Integer> numbers) {
    String type = numbers.getClass().getCanonicalName();
    console.print(String.format("Java type of numbers is %s", type));

    String reference = numbers.toString();
    console.print(String.format("So numbers %s is immutable ... ah, so that's why!", reference));
  }
}
