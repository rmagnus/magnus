package us.javaj.magnus.sample.demo;

import java.io.IOException;


public class JavaSevenExample {
  private static final String DEMO_FILE = "Sandbox/answer.txt";

  public static void main(String[] args) {
    ConsolePrinter console = new ConsolePrinter(777);

    try (final UserFileReader file = new UserFileReader(DEMO_FILE)) {
      file.lines().forEach(console::print);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    // finally {
    //   file.close(); no need since Java 7 and AutoCloseable!
    // }
  }
}
