package us.javaj.magnus.sample.demo;

import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;


@Component
public class ConsolePrinter {

  private PrintStream printer;
  private int millis;

  public ConsolePrinter() {
    this(System.out, 99);
  }

  public ConsolePrinter(int millis) {
    this(System.out, millis);
  }
  
  public ConsolePrinter(PrintStream printer, int millis) {
    this.printer = printer;
    this.millis = millis;
  }


  public void print(String line) {
    try {
      TimeUnit.MILLISECONDS.sleep(millis);
      printer.println(line);
    }
    catch(InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void print(Integer number) {
    print(Integer.toString(number));
  }
}
