package us.javaj.magnus.sample.demo;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;


public class JavaNineExample {

  @Autowired
  private ConsolePrinter console;

  private final UserFileReader autoCloseableFile;

  public JavaNineExample(UserFileReader userFile) {
    autoCloseableFile = userFile;
  }


  /**
   * Example of how the try-with-resources statement can now accept references,
   * such that Spring dependency injection can be used. Groovy mang!
   */
  public void demo() {
    try (autoCloseableFile) { // reference has been injected prior to calling!
      autoCloseableFile.lines().forEach(console::print);
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}
