package us.javaj.magnus.sample.demo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang3.StringUtils;


public class UserFileReader extends BufferedReader /* implements AutoCloseable */ {

  private static String fullPathToFile;

  public UserFileReader(String subpath) throws IOException {
    super(Files.newBufferedReader(pathWithSlash(subpath)));
  }


  /**
   * Closes this resource, relinquishing any underlying resources.
   *
   * This method is invoked automatically on objects managed by the
   * {@code try}-with-resources statement.
   */
  @Override
  public void close() throws IOException {
    System.out.println("Don't Panic! UserFileReader automagically closing down...");
    super.close();
  }


  private static Path pathWithSlash(String subpath) {
    boolean hasSlash = StringUtils.startsWith(subpath, "/");
    fullPathToFile = System.getProperty("user.home").concat(hasSlash ? subpath :  "/".concat(subpath));
    return Paths.get(fullPathToFile);
  }
}
