package us.javaj.magnus.sample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class DemoController implements TerribleInterface {

  @Override
  @ResponseBody
  @RequestMapping("/demo") // @RequestMapping(value="/", produces="text/plain")
  public String requestHandler(Model model) {
    return "<h1>Spring Boot 2.0 Rocks!</h1>" + appendSomeUglyMarkup(); // return "forward:/welcome";
  }


  @GetMapping("/restart")
  public String restartDemo(Model model) {
    return "forward:/welcome";
  }
}