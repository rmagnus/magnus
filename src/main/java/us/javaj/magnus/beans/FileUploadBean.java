package us.javaj.magnus.beans;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class FileUploadBean {

  @Value("${magnus.file-upload.max-size}")
  private long maxFileSizeMb;

  @Value("${magnus.file-upload.location}")
  private String uploadLocation;


  // Accessors
  public long getMaxFileSizeMb() {
    return maxFileSizeMb;
  }

  public long getMaxFileSizeBytes() {
    return maxFileSizeMb * 1024 * 1204;
  }

  public String getUploadLocation() {
    return uploadLocation;
  }

  public File getUploadLocationAsFile() {
    return new File(uploadLocation);
  }
}
