package us.javaj.magnus.beans;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.assertj.core.util.Arrays;


public class CommonProps {
  private static final Map<String, String> PROPS;


  public String getOrReflect(Object key) {
    return StringUtils.defaultString(get(key), key.toString());
  }

  public String get(Object key) {
    return PROPS.get(key.toString());
  }

  public String get(String key) {
    return PROPS.get(key);
  }

  public String get(String... keys) {
    StringBuilder sb = new StringBuilder();
    Arrays.asList(keys).forEach(key -> sb.append(getOrReflect(key)));
    return sb.toString();
  }


  static {
    PROPS = new HashMap<>();
    PROPS.put("site", "javaj.us");
    PROPS.put("middot", "&nbsp;&middot;&middot;&raquo;&nbsp;");
    // ---------------------------------------------------- //
    PROPS.put("homepage", "willkommen");
    PROPS.put("create-profile", "new profile");
    PROPS.put("create-welcome", "profile created");
    PROPS.put("sandbox-projects", "javaj projects");
    PROPS.put("sandbox-features", "display features");
  }
}
