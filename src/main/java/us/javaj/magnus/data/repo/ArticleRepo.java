package us.javaj.magnus.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import us.javaj.magnus.data.domain.Article;


public interface ArticleRepo extends JpaRepository<Article, Integer> {

}
