package us.javaj.magnus.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import us.javaj.magnus.data.domain.Message;


@Repository
public interface MessageRepo extends JpaRepository<Message, Integer> {
}
