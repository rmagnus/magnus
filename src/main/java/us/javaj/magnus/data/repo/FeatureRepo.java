package us.javaj.magnus.data.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import us.javaj.magnus.data.domain.Feature;


@Repository
public interface FeatureRepo extends JpaRepository<Feature, Integer> {

  @Query("select f from Feature f where f.article.id = :articleId")
  public Optional<Feature> findByArticleId(Integer articleId);

  public Optional<Feature> findByKeywordsContains(String keyword);

  public List<Feature> findByKeywordsContainsAndActive(String keyword, boolean active);

  public List<Feature> findByKeywordsContainsAndKeywordsContainsAndActive(String keyword1, String keyword2, boolean active);

}
