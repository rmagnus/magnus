package us.javaj.magnus.data.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import us.javaj.data.UniqueEntity;


@Entity
@Table(name = "JVJ_GROUP")
@AttributeOverride(column = @Column(name = "GROUP_ID"), name = "id")
public class Group extends UniqueEntity {

  @NotNull @Size(max = 64)
  @Column(name = "GROUP_NAME", nullable = false)
  private String name;

  @Column(name = "ACTIVE")
  private boolean active;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "PROFILE_ID", nullable = false)
  private Profile moderator;


  // Accessors
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public Profile getModerator() {
    return moderator;
  }

  public void setModerator(Profile moderator) {
    this.moderator = moderator;
  }
}
