package us.javaj.magnus.data.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import us.javaj.data.TimeSensitive;


@Entity
@Table(name = "JVJ_MESSAGE")
@AttributeOverride(column = @Column(name = "MESSAGE_ID"), name = "id")
public class Message extends TimeSensitive {

  @NotNull @Size(max = 280)
  @Column(name = "MARKDOWN", nullable = false)
  private String markdown;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "PROFILE_ID", nullable = false)
  private Profile profile;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  @JoinColumn(name = "FEATURE_ID", nullable = true)
  private Feature feature;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  @JoinColumn(name = "REPLY_ID", nullable = true)
  private Message topmostMessage;



  // Accessors
  public String getMarkdown() {
    return markdown;
  }

  public void setMarkdown(String markdown) {
    this.markdown = markdown;
  }

  public Profile getProfile() {
    return profile;
  }

  public void setProfile(Profile profile) {
    this.profile = profile;
  }

  public Feature getFeature() {
    return feature;
  }

  public void setFeature(Feature feature) {
    this.feature = feature;
  }

  public Message getTopmostMessage() {
    return topmostMessage;
  }

  public void setTopmostMessage(Message topmostMessage) {
    this.topmostMessage = topmostMessage;
  }
}
