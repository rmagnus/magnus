package us.javaj.magnus.data.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import us.javaj.data.UniqueEntity;


@Entity
@Table(name = "JVJ_ARTICLE")
@AttributeOverride(column = @Column(name = "ARTICLE_ID"), name = "id")
public class Article extends UniqueEntity {

  @NotNull @Size(max = 64)
  @Column(name = "ARTICLE_TITLE", nullable = false)
  private String title;

  @NotNull @Size(max = 32)
  @Column(name = "ARTICLE_URI", nullable = false)
  private String uri;

  @Column(name = "ACTIVE")
  private boolean active;

  
  // Accessors
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
