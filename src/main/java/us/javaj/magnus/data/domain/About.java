package us.javaj.magnus.data.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import us.javaj.data.UniqueEntity;


@Entity
@Table(name = "JVJ_ABOUT")
@AttributeOverride(column = @Column(name = "ABOUT_ID"), name = "id")
public class About extends UniqueEntity {

  @Size(max = 24)
  @Column(name = "PREPOSITION", nullable = false)
  private String preposition;

  @Size(max = 32)
  @Column(name = "TARGET", nullable = false)
  private String target;

  @Size(max = 128)
  @Column(name = "TARGET_URL")
  private String targetUrl;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "PROFILE_ID", nullable = false)
  private Profile profile;

  public About(String preposition) {
    this.preposition = preposition;
  }


  // Accessors
  public String getPreposition() {
    return preposition;
  }

  public void setPreposition(String preposition) {
    this.preposition = preposition;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public String getTargetUrl() {
    return targetUrl;
  }

  public void setTargetUrl(String targetUrl) {
    this.targetUrl = targetUrl;
  }

  public Profile getProfile() {
    return profile;
  }

  public void setProfile(Profile profile) {
    this.profile = profile;
  }
}
