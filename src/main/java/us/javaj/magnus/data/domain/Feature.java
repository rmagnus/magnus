package us.javaj.magnus.data.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import us.javaj.data.TimeSensitive;


@Entity
@Table(name = "JVJ_FEATURE")
@AttributeOverride(column = @Column(name = "FEATURE_ID"), name = "id")
public class Feature extends TimeSensitive {

  @Size(max = 128)
  @Column(name = "KEYWORDS")
  private String keywords;

  @Column(name = "MARKDOWN", nullable = false)
  private String markdown;

  @Size(max = 48)
  @Column(name = "FEATURE_TITLE")
  private String title;

  @Size(max = 64)
  @Column(name = "FEATURE_URI")
  private String uri;

  @Size(max = 128)
  @Column(name = "IMAGE_URL")
  private String imageUrl;

  @Size(max = 24)
  @Column(name = "POSITION")
  private String position;

  @Column(name = "SITEWIDE")
  private boolean sitewide;

  @Column(name = "ACTIVE")
  private boolean active;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  @JoinColumn(name = "ARTICLE_ID", nullable = true)
  private Article article;


  // Accessors
  public String getKeywords() {
    return keywords;
  }

  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getMarkdown() {
    return markdown;
  }

  public void setMarkdown(String markdown) {
    this.markdown = markdown;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public boolean isSitewide() {
    return sitewide;
  }

  public void setSitewide(boolean sitewide) {
    this.sitewide = sitewide;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public Article getArticle() {
    return article;
  }

  public void setArticle(Article article) {
    this.article = article;
  }
}
