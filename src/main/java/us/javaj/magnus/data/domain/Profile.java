package us.javaj.magnus.data.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import us.javaj.data.TimeSensitive;


@Entity
@Table(name = "JVJ_PROFILE")
@AttributeOverride(name="id", column = @Column(name="PROFILE_ID"))
public class Profile extends TimeSensitive {

  @Size(max = 16)
  @Column(name = "FIRST_NAME", nullable = false)
  private String firstName;

  @Size(max = 24)
  @Column(name = "LAST_NAME")
  private String lastName;

  @Size(max = 40)
  @Column(name = "DISPLAY_NAME")
  private String displayName;

  @Size(max = 128)
  @Column(name = "AVATAR_URL")
  private String avatarUrl;

  @Size(max = 128)
  @Column(name = "BACKDROP_URL")
  private String backdropUrl;
  
  @Size(max = 140)
  @Column(name = "PERSONAL_IDIOM")
  private String idiom;

  @Size(max = 16)
  @Column(name = "TWITTER_HANDLE")
  private String twitterHandle;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  @JoinColumn(name = "IDOL_ID", nullable = true)
  private Profile hero;


  // Accessors
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getAvatarUrl() {
    return avatarUrl;
  }

  public void setAvatarUrl(String avatarUrl) {
    this.avatarUrl = avatarUrl;
  }

  public String getBackdropUrl() {
    return backdropUrl;
  }

  public void setBackdropUrl(String backdropUrl) {
    this.backdropUrl = backdropUrl;
  }

  public String getIdiom() {
    return idiom;
  }

  public void setIdiom(String idiom) {
    this.idiom = idiom;
  }

  public String getTwitterHandle() {
    return twitterHandle;
  }

  public void setTwitterHandle(String twitterHandle) {
    this.twitterHandle = twitterHandle;
  }

  public Profile getHero() {
    return hero;
  }

  public void setHero(Profile hero) {
    this.hero = hero;
  }
}
