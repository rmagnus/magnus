package us.javaj.magnus.data;

import io.github.gitbucket.markedj.Marked;


public interface MarkdownCapable {

  public String getHtml();

  default String toHtml(String markdown) {
    return Marked.marked(markdown);
  }
}
