package us.javaj.magnus.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import us.javaj.exception.FileStorageException;
import us.javaj.magnus.beans.FileUploadBean;


@Service
public class FileStorageService {

  private Path tempUploadLocation;

  @Autowired
  public FileStorageService(FileUploadBean fileUpload) {
    tempUploadLocation = Paths.get(fileUpload.getUploadLocation()).toAbsolutePath().normalize();
    try {
      Files.createDirectories(tempUploadLocation);
    }
    catch (IOException ex) {
      throw new RuntimeException("Could not create directory where uploaded files will be stored.", ex);
    }
  }


  /**
   * Stores the passed multipart-file in a temporary upload location and returns
   * the endpoint to which it can be retrieved. Later the file must be placed in
   * a permanent location (database or Amazon S3 etc.) so that it can be reliably
   * retrieved beyond the lifecycle of the web server.
   *
   * @param file
   * @return retrievalEndpoint
   */
  public String storeMultipartFile(MultipartFile file) {
    String fileName = StringUtils.cleanPath(file.getOriginalFilename());

    try {
      // Check if the name contains invalid characters
      if (fileName.contains("..")) {
        throw new FileStorageException("Snap! File name contains invalid path sequence " + fileName);
      }

      // Copy file to the target location
      Path targetLocation = tempUploadLocation.resolve(fileName);
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

      return "/download/?".replace("?", fileName);
    }
    catch (IOException ex) {
      throw new FileStorageException("Could not store file " + fileName + ".", ex);
    }
  }


  public Resource loadFileAsResource(String fileName) {
    try {
      Path filePath = tempUploadLocation.resolve(fileName).normalize();
      Resource resource = new UrlResource(filePath.toUri());
      if (resource.exists()) {
        return resource;
      }
      else {
        throw new FileStorageException("File not found " + fileName);
      }
    }
    catch (MalformedURLException ex) {
      throw new FileStorageException("File not found " + fileName, ex);
    }
  }
}
