package us.javaj.magnus.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import us.javaj.exception.NotFoundException;
import us.javaj.magnus.data.domain.Article;
import us.javaj.magnus.data.domain.Feature;
import us.javaj.magnus.data.domain.Message;
import us.javaj.magnus.data.repo.ArticleRepo;
import us.javaj.magnus.data.repo.FeatureRepo;
import us.javaj.magnus.data.repo.MessageRepo;
import us.javaj.magnus.web.streams.StreamItemModel;


@Service
public class ApplicationService {

  @Autowired
  ArticleRepo articleRepo;
  @Autowired
  FeatureRepo featureRepo;
  @Autowired
  MessageRepo messageRepo;


  @Transactional(readOnly=true)
  public List<Feature> getFeatures() {
    return featureRepo.findAll();
  }
  
  @Transactional(readOnly=true)
  public StreamItemModel fetchFeatureModel(String keyword) {
    Optional<Feature> featureByKeywords = featureRepo.findByKeywordsContains(keyword);
    featureByKeywords.orElseThrow(() -> new NotFoundException(String.format("No feature using %s!", keyword)));

    return new StreamItemModel(featureByKeywords.get());
  }

  @Transactional(readOnly=true)
  public StreamItemModel fetchRandomAuthorQuoteFeatureModel(String author) {
    List<Feature> featureByKeywords = featureRepo.findByKeywordsContainsAndKeywordsContainsAndActive("author", author, true);

    return new StreamItemModel(featureByKeywords.get(0));
  }

  @Transactional(readOnly=true)
  public StreamItemModel fetchFeatureArticle(Integer articleId) {
    Optional<Feature> featuredArticle = featureRepo.findByArticleId(articleId);
    featuredArticle.orElseThrow(() -> new NotFoundException(String.format("No featured article %s!", articleId)));

    return new StreamItemModel(featuredArticle.get());
  }

  @Transactional
  public Feature saveJavajFeature(Feature feature) {
    return featureRepo.save(feature);
  }


  @Transactional(readOnly=true)
  public List<Message> getMessages() {
    return messageRepo.findAll();
  }

  
  @Transactional(readOnly=true)
  public Message getMessage(Integer messageId) throws NotFoundException {
    return messageRepo.findById(messageId)
        .orElseThrow(() -> new NotFoundException("Message not found!"));
  }

  @Transactional
  public Message saveJavajMessage(Message message) {
    return messageRepo.save(message);
  }


  @Transactional(readOnly=true)
  public Article getArticle(Integer articleId) throws NotFoundException {
    return articleRepo.findById(articleId)
        .orElseThrow(() -> new NotFoundException("Article not found!"));
  }

  @Transactional
  public Article saveJavajArticle(Article article) {
    return articleRepo.save(article);
  }
}