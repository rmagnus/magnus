package us.javaj.magnus.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import us.javaj.exception.NotFoundException;
import us.javaj.magnus.data.domain.Profile;
import us.javaj.magnus.data.repo.AboutRepo;
import us.javaj.magnus.data.repo.ProfileRepo;
import us.javaj.magnus.web.profile.ProfileFormModel;
import us.javaj.magnus.web.profile.ProfileModel;


@Service
public class CreateProfileService {

  @Autowired
  AboutRepo aboutRepo;
  @Autowired
  ProfileRepo profileRepo;


  @Transactional(readOnly=true)
  public List<Profile> getProfiles() {
    return profileRepo.findAll();
  }

  @Transactional(readOnly=true)
  public Profile getProfile(Integer profileId) throws NotFoundException {
    return profileRepo.findById(profileId)
        .orElseThrow(() -> new NotFoundException("Profile not found!"));
  }

  @Transactional(readOnly=true)
  public ProfileModel fetchProfileModel(Integer id) {
    return new ProfileModel(getProfile(id));
  }

  @Transactional
  public Profile createNewProfile(ProfileFormModel model) {
    final Profile profile = new Profile();
    profile.setDisplayName(model.getName());
    profile.setIdiom(model.getIdiom());
    profile.setBackdropUrl(model.getBackgroundEndpoint());
    profile.setAvatarUrl(model.getAvatarEndpoint());
    profileRepo.save(profile);

    // save abouts if any
    model.getAbouts().forEach(ab -> {
      if (StringUtils.isNotBlank(ab.getTarget())) {
        ab.setProfile(profile);
        aboutRepo.save(ab);
      }
    });

    return profile;
  }
}
