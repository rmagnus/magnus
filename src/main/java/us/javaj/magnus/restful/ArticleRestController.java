package us.javaj.magnus.restful;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import us.javaj.magnus.data.domain.Article;
import us.javaj.magnus.service.ApplicationService;


@RestController
@RequestMapping("/javaj")
public class ArticleRestController {

  @Autowired
  private ApplicationService appService;


  /**
   * Bind and create a new message for display on the homepage.
   *
   * @param profileId
   * @param message
   * @return Message
   */
  @PostMapping("/articles")
  public Article createArticle(@Valid @RequestBody Article article) {
    return appService.saveJavajArticle(article);
  }
}
