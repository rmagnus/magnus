package us.javaj.magnus.restful;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import us.javaj.exception.NotFoundException;
import us.javaj.exception.ParamManipulationException;
import us.javaj.magnus.data.domain.Article;
import us.javaj.magnus.data.domain.Feature;
import us.javaj.magnus.data.domain.Message;
import us.javaj.magnus.service.ApplicationService;


@RestController
@RequestMapping("/javaj")
public class FeatureRestController {

  @Autowired
  private ApplicationService appService;

  
  /**
   * Return JSON representation of all homepage messages.
   *
   * @return messages
   */
  @GetMapping("/features")
  public List<Feature> getHomepageFeatures() {
    return appService.getFeatures();
  }


  /**
   * Bind and create a new feature for display on the homepage.
   *
   * @param profileId
   * @param message
   * @return Message
   */
  @PostMapping("/features")
  public Feature createFeature(@Valid @RequestBody Feature feature) {
    return appService.saveJavajFeature(feature);
  }


  /**
   * Bind and create a new featured message for display on the homepage.
   * The message must exist first after any edits are made and is ready
   * for 'publishing' to the homepage.
   *
   * @param messageId
   * @param feature
   * @return Message
   */
  @PostMapping("/messages/{messageId}/features")
  public Feature createFeaturedMessage(@PathVariable("messageId") Integer messageId,
      @Valid @RequestBody Feature feature) {
    try {
      Message message = appService.getMessage(messageId);
      feature.setMarkdown(message.getMarkdown());
      //feature.setMessage(message);
    }
    catch (NotFoundException ex) {
      throw new ParamManipulationException("OWASP top ten!", ex);
    }
    return appService.saveJavajFeature(feature);
  }


  /**
   * Bind and create a new featured article for display on the homepage.
   * The article entry must exist for 'publishing' to the homepage.
   *
   * @param articleId
   * @param feature
   * @return Feature
   */
  @PostMapping("/articles/{articleId}/features")
  public Feature createFeaturedArticle(@PathVariable("articleId") Integer articleId,
      @Valid @RequestBody Feature feature) {
    try {
      Article article = appService.getArticle(articleId);
      feature.setArticle(article);
    }
    catch (NotFoundException ex) {
      throw new ParamManipulationException("OWASP top ten!", ex);
    }
    return appService.saveJavajFeature(feature);
  }
}
