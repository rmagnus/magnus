package us.javaj.magnus.restful;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import us.javaj.magnus.data.domain.Profile;
import us.javaj.magnus.service.CreateProfileService;


@RestController
@RequestMapping("/javaj")
public class ProfileRestController {

  @Autowired
  private CreateProfileService profileService;

  
  /**
   * Return JSON representation of all JavaJ profiles.
   *
   * @return profiles
   */
  @GetMapping("/profiles")
  public List<Profile> getJavajProfiles() {
    return profileService.getProfiles();
  }
}
