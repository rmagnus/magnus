package us.javaj.magnus.restful;


import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import us.javaj.exception.NotFoundException;
import us.javaj.exception.ParamManipulationException;
import us.javaj.magnus.data.domain.Message;
import us.javaj.magnus.data.domain.Profile;
import us.javaj.magnus.service.ApplicationService;
import us.javaj.magnus.service.CreateProfileService;


@RestController
@RequestMapping("/javaj")
public class MessageRestController {

  @Autowired
  private ApplicationService appService;

  @Autowired
  private CreateProfileService profileService;


  /**
   * Bind and create a new message for display on the homepage.
   *
   * @param profileId
   * @param message
   * @return Message
   */
  @PostMapping("/profiles/{profileId}/messages")
  public Message createHomepageMessage(@PathVariable("profileId") Integer profileId,
      @Valid @RequestBody Message message) {
    try {
      Profile profile = profileService.getProfile(profileId);
      message.setProfile(profile);
    }
    catch (NotFoundException ex) {
      throw new ParamManipulationException("OWASP top ten!", ex);
    }
    return appService.saveJavajMessage(message);
  }
}
