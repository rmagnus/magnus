package us.javaj.exception;


public class NotFoundException extends RuntimeException {
  private static final long serialVersionUID = -8211580167806189039L;

  public NotFoundException(String message) {
    super(message);
  }
}
