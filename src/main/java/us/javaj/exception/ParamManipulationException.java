package us.javaj.exception;


public class ParamManipulationException extends RuntimeException {
  private static final long serialVersionUID = -5015449790745429505L;

  public ParamManipulationException(String message, Throwable cause) {
    super(message, cause);
  }
}
