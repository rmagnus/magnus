package us.javaj.exception;


public class FileStorageException extends RuntimeException {
  private static final long serialVersionUID = 8247001616489533070L;

  public FileStorageException(String message) {
    super(message);
  }

  public FileStorageException(String message, Throwable cause) {
    super(message, cause);
  }
}
