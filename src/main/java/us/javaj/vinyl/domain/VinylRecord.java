package us.javaj.vinyl.domain;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import us.javaj.data.UniqueEntity;
import us.javaj.data.WebSavvy;


@Entity
@Table(name = "BNP_VINYL")
@AttributeOverride(name="id", column = @Column(name="VINYL_ID"))
public class VinylRecord extends UniqueEntity implements WebSavvy {

  @NotNull
  @Column(name = "RECORD_TITLE", nullable = false)
  private String title;

  @Column(name = "RECORD_LABEL")
  private String label;

  @Column(name = "RECORD_NOTES")
  private String notes;

  @Column(name = "DISPLAY_ORDER")
  private Integer displayOrder;

  @NotNull
  @Column(name = "YEAR_RELEASE")
  private String releaseYear;

  @Column(name = "YEAR_REISSUE")
  private String reissueYear;

  @Column(name = "DISCOGS_LINK")
  private String link;

  @JsonIgnore
  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "vinylRecord")
  private List<SongTrack> tracks;

  @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "ARTIST_ID", nullable = false)
  private Artist artist;


  // Accessors
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public Integer getDisplayOrder() {
    return displayOrder;
  }

  public void setDisplayOrder(Integer displayOrder) {
    this.displayOrder = displayOrder;
  }

  public String getReleaseYear() {
    return releaseYear;
  }

  public void setReleaseYear(String releaseYear) {
    this.releaseYear = releaseYear;
  }

  public String getReissueYear() {
    return reissueYear;
  }

  public void setReissueYear(String reissueYear) {
    this.reissueYear = reissueYear;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public Artist getArtist() {
    return artist;
  }

  public void setArtist(Artist artist) {
    this.artist = artist;
  }

  public List<SongTrack> getTracks() {
    return tracks;
  }

  public void setTracks(List<SongTrack> tracks) {
    this.tracks = tracks;
  }
}
