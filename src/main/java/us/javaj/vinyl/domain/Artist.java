package us.javaj.vinyl.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import us.javaj.data.UniqueEntity;


@Entity
@Table(name = "BNP_ARTIST")
@AttributeOverride(name="id", column = @Column(name="ARTIST_ID"))
public class Artist extends UniqueEntity {

  @NotNull @Size(max = 32)
  @Column(name = "ARTIST_NAME", nullable = false)
  private String name;

  @NotNull @Size(max = 128)
  @Column(name = "ARTIST_WEBSITE")
  private String website;

  @Column(name = "YEARS_ACTIVE")
  private String yearsActive;

  @Column(name = "WIKI_ARTICLE")
  private String wiki;


  // Accessors
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getYearsActive() {
    return yearsActive;
  }

  public void setYearsActive(String yearsActive) {
    this.yearsActive = yearsActive;
  }

  public String getWiki() {
    return wiki;
  }

  public void setWiki(String wiki) {
    this.wiki = wiki;
  }
}
