package us.javaj.vinyl.domain;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import us.javaj.data.UniqueEntity;


@Entity
@Table(name = "BNP_TRACKS")
@AttributeOverride(name="id", column = @Column(name="TRACK_ID"))
public class SongTrack extends UniqueEntity {

  @NotNull @Min(1)
  @Column(name = "TRACK_NO")
  private Integer number;

  @NotNull @Size(max = 48)
  @Column(name = "TRACK_TITLE", nullable = false)
  private String title;

  @Column(name = "TRACK_LENGTH")
  private String length;

  @Column(name = "TRACK_NOTES")
  private String notes;

  @JsonIgnore
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "VINYL_ID", nullable = false)
  private VinylRecord vinylRecord;


  // Accessors
  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLength() {
    return length;
  }

  public void setLength(String length) {
    this.length = length;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public VinylRecord getVinylRecord() {
    return vinylRecord;
  }

  public void setVinylRecord(VinylRecord vinylRecord) {
    this.vinylRecord = vinylRecord;
  }
}
