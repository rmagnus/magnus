package us.javaj.vinyl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import us.javaj.exception.NotFoundException;
import us.javaj.vinyl.domain.Artist;
import us.javaj.vinyl.domain.SongTrack;
import us.javaj.vinyl.domain.VinylRecord;
import us.javaj.vinyl.repo.ArtistRepo;
import us.javaj.vinyl.repo.SongTrackRepo;
import us.javaj.vinyl.repo.VinylRecordRepo;


@Service
public class VinylCollectionService {

  @Autowired
  ArtistRepo artistRepo;

  @Autowired
  VinylRecordRepo vinylRepo;

  @Autowired
  SongTrackRepo trackRepo;


  @Transactional(readOnly=true)
  public List<Artist> getArtists() {
    return artistRepo.findAll();
  }

  @Transactional(readOnly=true)
  public Artist getArtist(Integer artistId) throws NotFoundException {
    return artistRepo.findById(artistId)
        .orElseThrow(() -> new NotFoundException("Artist not found!"));
  }

  @Transactional
  public Artist saveArtist(Artist artist) {
    return artistRepo.save(artist);
  }


  @Transactional(readOnly=true)
  public List<VinylRecord> getVinylRecords() {
    return vinylRepo.findAll();
  }

  /**
   * Get a vinyl record by vinylId. If the vinyl does not exist
   * for the passed identifier, throw runtime NotFoundException.
   * 
   * @param trackId
   * @return songTrack
   */
  @Transactional(readOnly=true)
  public VinylRecord getVinylRecord(Integer vinylId) throws NotFoundException {
    return vinylRepo.findById(vinylId)
        .orElseThrow(() -> new NotFoundException("Vinyl not found!"));
  }

  /**
   * Save or update the passed vinyl record.
   * 
   * @param vinyl
   * @return vinyl
   */
  @Transactional
  public VinylRecord saveVinylRecord(VinylRecord vinyl) {
    return vinylRepo.save(vinyl);
  }

  /**
   * Save or update the passed song track.
   * 
   * @param songTrack
   * @return songTrack
   */
  @Transactional
  public SongTrack saveSongTrack(SongTrack songTrack) {
    return trackRepo.save(songTrack);
  }

  /**
   * Update the values for the song track identified by passed trackId
   * using the values from the passed song track. Return modified track.
   *
   * @param trackId
   * @param songTrack
   *
   * @return SongTrack
   */
  @Transactional
  public SongTrack updateSongTrack(Integer trackId, SongTrack updates) {
    SongTrack songTrack = trackRepo.getOne(trackId);
    songTrack.setNumber(updates.getNumber());
    songTrack.setTitle(updates.getTitle());
    songTrack.setLength(updates.getLength());
    songTrack.setNotes(updates.getNotes());

    return trackRepo.save(songTrack);
  }

  /**
   * Get all song tracks for the vinyl record specified by vinylId.
   * 
   * @param vinylId
   * @return songTracks
   */
  @Transactional(readOnly=true)
  public List<SongTrack> getSongTracks(Integer vinylId) {
    return trackRepo.findByVinylRecordIdOrderByNumber(vinylId);
  }

  /**
   * Get a song track by trackId. If the track does not exist
   * for the passed trackId, throw runtime NotFoundException.
   * 
   * @param trackId
   * @return songTrack
   */
  @Transactional(readOnly=true)
  public SongTrack getSongTrack(Integer trackId) throws NotFoundException {
    return trackRepo.findById(trackId)
        .orElseThrow(() -> new NotFoundException("Track not found!"));
  }

  /**
   * Get a song track by trackId and vinylId. If the track does not
   * exist for passed identifiers, throw runtime NotFoundException.
   * 
   * @param trackId
   * @param vinylId
   * @return
   */
  @Transactional(readOnly=true)
  public SongTrack getSongTrackForVinyl(Integer trackId, Integer vinylId)
    throws NotFoundException {
    if (trackRepo.findByIdAndVinylRecordId(trackId, vinylId).isEmpty()) {
      throw new NotFoundException("Track not found for record!");
    }
    return getSongTrack(trackId);
  }
}
