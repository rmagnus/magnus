package us.javaj.vinyl.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import us.javaj.exception.NotFoundException;
import us.javaj.exception.ParamManipulationException;
import us.javaj.vinyl.domain.SongTrack;
import us.javaj.vinyl.domain.VinylRecord;
import us.javaj.vinyl.service.VinylCollectionService;


@RestController
@RequestMapping("/vinyl")
public class RecordRestController {

  @Autowired
  private VinylCollectionService vinylCollection;


  /**
   * Return JSON representation of the specified vinyl record.
   * 
   * @param vinylId
   * @return VinylRecord
   * @throws NotFoundException 
   */
  @GetMapping("/records/{vinylId}")
  public VinylRecord getVinylRecord(@PathVariable("vinylId") Integer vinylId) throws NotFoundException {
    return vinylCollection.getVinylRecord(vinylId);
  }


  /**
   * Return JSON representation of all the song tracks for specified vinyl record.
   * 
   * @param vinylId
   * @return song tracks
   */
  @GetMapping("/records/{vinylId}/tracks")
  public List<SongTrack> getSongTracks(@PathVariable("vinylId") Integer vinylId) {
    return vinylCollection.getSongTracks(vinylId);
  }


  /**
   * Bind and create a brand new song track for the specified vinyl record.
   * 
   * @param vinylId
   * @param songTrack
   * @return songTrack
   */
  @PostMapping("/records/{vinylId}/tracks")
  public SongTrack createSongTrack(@PathVariable("vinylId") Integer vinylId,
    @Valid @RequestBody SongTrack songTrack) {
    try {
      VinylRecord realVinylRecord = vinylCollection.getVinylRecord(vinylId);
      songTrack.setVinylRecord(realVinylRecord);
    }
    catch (NotFoundException ex) {
      throw new ParamManipulationException("OWASP top ten!", ex);
    }
    return vinylCollection.saveSongTrack(songTrack);
  }


  /**
   * Update an existing song track with edits for the specified vinyl record.
   *
   * @param vinylId
   * @param trackId
   * @param songTrack
   * @return
   */
  @PutMapping("/records/{vinylId}/tracks/{trackId}")
  public SongTrack updateSongTrack(@PathVariable("vinylId") Integer vinylId,
    @PathVariable("trackId") Integer trackId,
    @Valid @RequestBody SongTrack songTrack) {
    try {
      vinylCollection.getSongTrackForVinyl(trackId, vinylId);
    }
    catch (NotFoundException ex) {
      throw new ParamManipulationException("OWASP top ten!", ex);
    }
    return vinylCollection.updateSongTrack(trackId, songTrack);
  }
}
