package us.javaj.vinyl.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import us.javaj.exception.NotFoundException;
import us.javaj.exception.ParamManipulationException;
import us.javaj.vinyl.domain.Artist;
import us.javaj.vinyl.domain.VinylRecord;
import us.javaj.vinyl.service.VinylCollectionService;


@RestController
@RequestMapping("/vinyl")
public class ArtistRestController {

  @Autowired
  private VinylCollectionService vinylCollection;

  
  /**
   * Return JSON representation of all vinyl record artists.
   * 
   * @param vinylId
   * @return song tracks
   */
  @GetMapping("/artists")
  public List<Artist> getRecordingArtists() {
    return vinylCollection.getArtists();
  }


  /**
   * Bind and create a new vinyl record collection artist.
   * 
   * @param artist
   * @return Artist
   */
  @PostMapping("/artists")
  public Artist createRecordingArtist(@Valid @RequestBody Artist artist) {
    return vinylCollection.saveArtist(artist);
  }


  /**
   * Return JSON representation of the specified artist.
   * 
   * @param artistId
   * @return Artist
   * @throws NotFoundException 
   */
  @GetMapping("/artists/{artistId}")
  public Artist getRecordingArtist(@PathVariable("artistId") Integer artistId) throws NotFoundException {
    return vinylCollection.getArtist(artistId);
  }
  
  
  /**
   * Bind and create a new vinyl record entry for the specified artist.
   * 
   * @param vinylId
   * @return VinylRecord
   */
  @PostMapping("/artists/{artistId}/records")
  public VinylRecord createVinylRecord(@PathVariable("artistId") Integer artistId,
    @Valid @RequestBody VinylRecord vinylRecord) {
    try {
      Artist artist = vinylCollection.getArtist(artistId);
      vinylRecord.setArtist(artist);
    }
    catch (NotFoundException ex) {
      throw new ParamManipulationException("OWASP top ten!", ex);
    }
    
    return vinylCollection.saveVinylRecord(vinylRecord);
  }
}
