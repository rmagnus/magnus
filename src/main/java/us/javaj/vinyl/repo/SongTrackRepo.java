package us.javaj.vinyl.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import us.javaj.vinyl.domain.SongTrack;


@Repository
public interface SongTrackRepo extends JpaRepository<SongTrack, Integer> {

  List<SongTrack> findByIdAndVinylRecordId(Integer trackId, Integer vinylId);
  
  List<SongTrack> findByVinylRecordIdOrderByNumber(Integer vinylId);

  List<SongTrack> findByVinylRecordIdOrderByNumberDesc(Integer vinylId);

}
