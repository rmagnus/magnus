package us.javaj.vinyl.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import us.javaj.vinyl.domain.Artist;


@Repository
public interface ArtistRepo extends JpaRepository<Artist, Integer> {

}
