package us.javaj.vinyl.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import us.javaj.vinyl.domain.VinylRecord;


@Repository
public interface VinylRecordRepo extends JpaRepository<VinylRecord, Integer> {

}
