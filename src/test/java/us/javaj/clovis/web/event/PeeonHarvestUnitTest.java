package us.javaj.clovis.web.event;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.Test;

import us.javaj.clovis.GameException;
import us.javaj.clovis.beans.PlayerCard;
import us.javaj.clovis.domain.Fiefdom;
import us.javaj.clovis.domain.Player;


public class PeeonHarvestUnitTest {

  @Test
  public void testPlayerOne() throws GameException {
    PlayerCard gameCard = new PlayerCard(setupTestPlayerOne());

    PeeonHarvest harvest = new PeeonHarvest();
    harvest.applyPreAction(gameCard);
    int bales = harvest.getBales();

    // 1 = 625, 2 = 500, 3 = 250, 4 = 125
    assertTrue(List.of(625, 500, 250, 125).contains(bales));
  }


  @Test
  public void testPlayerTwo() throws GameException {
    PlayerCard gameCard = new PlayerCard(setupTestPlayerTwo());

    PeeonHarvest harvest = new PeeonHarvest();
    harvest.applyPreAction(gameCard);
    int bales = harvest.getBales();

    // 1 = 1112, 2 = 890, 3 = 445, 4 = 222
    assertTrue(List.of(1112, 890, 445, 222).contains(bales));
  }
  
  
  private Player setupTestPlayerOne() {
    Fiefdom fief = new Fiefdom();
    fief.setAkers(1200);
    fief.setPeons(500);

    Player readyPlayerOne = new Player();
    readyPlayerOne.setFiefdom(fief);
    return readyPlayerOne;
  }


  private Player setupTestPlayerTwo() {
    Fiefdom fief = new Fiefdom();
    fief.setAkers(450);
    fief.setPeons(890);

    Player readyPlayerTwo = new Player();
    readyPlayerTwo.setFiefdom(fief);
    return readyPlayerTwo;
  }
}
