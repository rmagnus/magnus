package us.javaj.magnus.data.domain;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.Test;

import us.javaj.magnus.web.streams.StreamItemModel;


/**
 * Used to test the various flavors of JavaJ Features.
 */
public class FeatureModelUnitTest {

  @Test
  public void testFeatureType() {
    // Feature
    Feature feature = mock(Feature.class);
    when(feature.getMarkdown()).thenReturn("Some **bold** text");
    when(feature.getDateCreated()).thenReturn(new Date());

    StreamItemModel model = new StreamItemModel(feature);
    assertThat(model.getHtml(), containsString("<strong>bold</strong>"));
    assertFalse(model.isMessage());
    assertFalse(model.isArticle());
    assertTrue(model.isFeature());
  }

  @Test
  public void testMessageType() {
    // Message
    Profile profile = mock(Profile.class);
    Message message = mock(Message.class);
    Feature featureMessage = mock(Feature.class);
    when(message.getProfile()).thenReturn(profile);
    //when(featureMessage.getMessage()).thenReturn(message);

    when(message.getDateCreated()).thenReturn(new Date());
    when(profile.getDisplayName()).thenReturn("Harry Potter");
    when(message.getMarkdown()).thenReturn("Some *magic* from Hogwarts");

    StreamItemModel messageModel = new StreamItemModel(featureMessage);
    assertThat(messageModel.getTitle(), equalTo("Harry Potter"));
    assertThat(messageModel.getHtml(), containsString("<em>magic</em>"));
  }

  @Test
  public void testArticleType() {
    // Article
    Article article = mock(Article.class);
    when(article.getTitle()).thenReturn("King Floyd");

    Feature featureArticle = mock(Feature.class);
    when(featureArticle.getArticle()).thenReturn(article);
    when(featureArticle.getTitle()).thenReturn("Read this article!");
    when(featureArticle.getMarkdown()).thenReturn("This is the story of...");
    when(featureArticle.getDateCreated()).thenReturn(new Date());

    StreamItemModel articleModel = new StreamItemModel(featureArticle);
    assertFalse(articleModel.getTitle().equals("King Floyd"));
    assertThat(articleModel.getHtml(), containsString("story of..."));
  }
}
